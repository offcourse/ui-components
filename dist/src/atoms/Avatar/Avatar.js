"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lower_case = require("voca/lower_case");

var _lower_case2 = _interopRequireDefault(_lower_case);

var _AvatarWrapper = require("./AvatarWrapper");

var _AvatarWrapper2 = _interopRequireDefault(_AvatarWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @name Avatar
 * @description a react component that shows the avatar image of a given user
 */

var Avatar = function (_Component) {
  _inherits(Avatar, _Component);

  function Avatar() {
    _classCallCheck(this, Avatar);

    return _possibleConstructorReturn(this, (Avatar.__proto__ || Object.getPrototypeOf(Avatar)).apply(this, arguments));
  }

  _createClass(Avatar, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          url = _props.url,
          name = _props.name;

      return _react2.default.createElement(_AvatarWrapper2.default, { src: url, alt: "avatar of " + (0, _lower_case2.default)(name) });
    }
  }]);

  return Avatar;
}(_react.Component);

Avatar.propTypes = {
  /**
   * @property {string} url that refers to an avatar image
   * */
  url: _propTypes2.default.string,
  /**
   * @property {string} name of the person corresponding to this avatar
   */
  name: _propTypes2.default.string.isRequired
};

Avatar.defaultProps = {
  url: "https://assets.offcourse.io/portraits/offcourse_2.jpg"
};

exports.default = Avatar;

//# sourceMappingURL=Avatar.js.map