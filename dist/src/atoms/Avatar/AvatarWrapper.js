"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _systemComponents = require("system-components");

var _systemComponents2 = _interopRequireDefault(_systemComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AvatarWrapper = (0, _systemComponents2.default)({
  is: "img",
  size: "3em"
});

exports.default = AvatarWrapper;

//# sourceMappingURL=AvatarWrapper.js.map