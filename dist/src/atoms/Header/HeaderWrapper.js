"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _systemComponents = require("system-components");

var _systemComponents2 = _interopRequireDefault(_systemComponents);

var _styledSystem = require("styled-system");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HeaderWrapper = (0, _systemComponents2.default)({
  m: 0,
  color: "inherit",
  lineHeight: 2,
  fontSize: 3
}, function (props) {
  return {
    fontFamily: props.theme.fonts.bold,
    userSelect: "none",
    textDecoration: "inherit",
    cursor: props.href ? "pointer" : "cursor",
    "&:hover": {
      color: props.href ? (0, _styledSystem.theme)("colors.primary")(props) : (0, _styledSystem.theme)("colors.black")(props)
    }
  };
});

exports.default = HeaderWrapper;

//# sourceMappingURL=HeaderWrapper.js.map