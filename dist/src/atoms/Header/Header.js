"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _title_case = require("voca/title_case");

var _title_case2 = _interopRequireDefault(_title_case);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _HeaderWrapper = require("./HeaderWrapper");

var _HeaderWrapper2 = _interopRequireDefault(_HeaderWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @name Header
 * @description Header Component
 */

var Header = function (_Component) {
  _inherits(Header, _Component);

  function Header() {
    _classCallCheck(this, Header);

    return _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).apply(this, arguments));
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          children = _props.children,
          href = _props.href,
          size = _props.size;

      return _react2.default.createElement(
        _HeaderWrapper2.default,
        {
          is: href ? "a" : "h1",
          href: href,
          lineHeight: size === "small" ? 1 : 2,
          fontSize: size === "small" ? 2 : 3
        },
        (0, _title_case2.default)(children)
      );
    }
  }]);

  return Header;
}(_react.Component);

Header.propTypes = {
  /**
   * @property {string} children The actual text of the header
   */
  children: _propTypes2.default.string.isRequired,
  /**
   * @property {string} size Field that indicates the size of the header
   */
  size: _propTypes2.default.oneOf(["small", "regular"]),
  /**
   * @property {string} href Headers can optionally link to other documents, etc
   */
  href: _propTypes2.default.string
};

Header.defaultProps = {
  size: "regular"
};

exports.default = Header;

//# sourceMappingURL=Header.js.map