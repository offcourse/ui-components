"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Outer = require("./Outer");

Object.defineProperty(exports, "OuterWrapper", {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Outer).default;
  }
});

var _Label = require("./Label");

Object.defineProperty(exports, "LabelWrapper", {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Label).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//# sourceMappingURL=index.js.map