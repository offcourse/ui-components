"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(["\n  grid-template-areas: \"checkbox\";\n\n  > input[type=\"checkbox\"] {\n    display: none;\n  }\n\n  > input[type=\"checkbox\"]:checked + label {\n    display: block;\n  }\n"], ["\n  grid-template-areas: \"checkbox\";\n\n  > input[type=\"checkbox\"] {\n    display: none;\n  }\n\n  > input[type=\"checkbox\"]:checked + label {\n    display: block;\n  }\n"]);

var _systemComponents = require("system-components");

var _systemComponents2 = _interopRequireDefault(_systemComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var OuterWrapper = (0, _systemComponents2.default)({
  size: "1.25rem",
  display: "grid",
  bg: "white",
  justifyContent: "center",
  alignItems: "center"
}).extend(_templateObject);

exports.default = OuterWrapper;

//# sourceMappingURL=Outer.js.map