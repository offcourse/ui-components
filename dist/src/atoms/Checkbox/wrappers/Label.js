"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(["\n  grid-area: checkbox;\n  box-sizing: border-box;\n"], ["\n  grid-area: checkbox;\n  box-sizing: border-box;\n"]);

var _systemComponents = require("system-components");

var _systemComponents2 = _interopRequireDefault(_systemComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LabelWrapper = (0, _systemComponents2.default)({
  is: "label",
  size: "1rem",
  display: "none",
  bg: "white",
  color: "black",
  hover: {
    color: "black"
  }
}, "space").extend(_templateObject);

exports.default = LabelWrapper;

//# sourceMappingURL=Label.js.map