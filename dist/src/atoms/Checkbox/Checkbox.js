"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIconsKit = require("react-icons-kit");

var _reactIconsKit2 = _interopRequireDefault(_reactIconsKit);

var _check = require("react-icons-kit/fa/check");

var _wrappers = require("./wrappers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @name Checkbox
 * @description Checkbox component for the offcourse project
 * @author Yeehaa
 */

var Checkbox = function (_Component) {
  _inherits(Checkbox, _Component);

  function Checkbox() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Checkbox);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      checked: _this.props.checked
    }, _this.handleClick = function (e) {
      e.preventDefault();
      var onToggle = _this.props.onToggle;

      _this.setState(function (_ref2) {
        var checked = _ref2.checked;
        return { checked: !checked };
      }, function () {
        return onToggle(_this.state);
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Checkbox, [{
    key: "render",
    value: function render() {
      var checked = this.state.checked;

      return _react2.default.createElement(
        _wrappers.OuterWrapper,
        { onClick: this.handleClick },
        _react2.default.createElement("input", { type: "checkbox", readOnly: true, checked: checked }),
        _react2.default.createElement(
          _wrappers.LabelWrapper,
          null,
          _react2.default.createElement(_reactIconsKit2.default, { icon: _check.check })
        )
      );
    }
  }]);

  return Checkbox;
}(_react.Component);

Checkbox.propTypes = {
  /**
   * @property {bool} checked allows the user to directly decide if the value is checked or not
   */
  checked: _propTypes2.default.bool,
  /**
   * @property {function} ontoggle is an optional callback that triggers when the checkbox changes its value
   */
  onToggle: _propTypes2.default.func
};

Checkbox.defaultProps = {
  checked: false,
  onToggle: function onToggle() {
    return null;
  }
};

exports.default = Checkbox;

//# sourceMappingURL=Checkbox.js.map