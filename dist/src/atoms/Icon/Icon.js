"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ramda = require("ramda");

var _reactIconsKit = require("react-icons-kit");

var _reactIconsKit2 = _interopRequireDefault(_reactIconsKit);

var _check = require("react-icons-kit/fa/check");

var _asterisk = require("react-icons-kit/fa/asterisk");

var _bars = require("react-icons-kit/fa/bars");

var _times = require("react-icons-kit/fa/times");

var _plus = require("react-icons-kit/fa/plus");

var _sort = require("react-icons-kit/fa/sort");

var _eye = require("react-icons-kit/fa/eye");

var _facebook = require("react-icons-kit/fa/facebook");

var _twitter = require("react-icons-kit/fa/twitter");

var _IconWrapper = require("./IconWrapper");

var _IconWrapper2 = _interopRequireDefault(_IconWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var icons = {
  facebook: _facebook.facebook,
  twitter: _twitter.twitter,
  asterisk: _asterisk.asterisk,
  sort: _sort.sort,
  eye: _eye.eye,
  remove: _times.times,
  add: _plus.plus,
  hamburger: Bars
};

/**
 * @name Icon
 * @description Icon Component
 */

var Icon = function (_Component) {
  _inherits(Icon, _Component);

  function Icon() {
    _classCallCheck(this, Icon);

    return _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).apply(this, arguments));
  }

  _createClass(Icon, [{
    key: "icon",
    value: function icon() {
      var name = this.props.name;

      return icons[name];
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          size = _props.size,
          spin = _props.spin,
          tabIndex = _props.tabIndex,
          name = _props.name,
          href = _props.href,
          is = _props.is,
          onClick = _props.onClick;

      return _react2.default.createElement(
        _IconWrapper2.default,
        {
          is: is || href && "a" || onClick && "button",
          type: is === "button" && "button" || onClick && "button",
          tabIndex: tabIndex,
          onClick: onClick
        },
        _react2.default.createElement(_reactIconsKit2.default, { icon: this.icon(), size: size, spin: false })
      );
    }
  }]);

  return Icon;
}(_react.Component);

var iconNames = (0, _ramda.keys)(icons);

Icon.propTypes = {
  /**
   * @property {string} name name of the icon
   */
  name: _propTypes2.default.oneOf(iconNames),
  /**
   * @property {string} size size of the icon
   */
  size: _propTypes2.default.oneOf(["xs", "sm", "lg", "2x", "3x", "4x", "5x", "6x", "7x", "8x", "9x", "10x"]),
  /**
   * @property {string} href url that the button should link to, automatically turns the button into the basic type
   */
  href: _propTypes2.default.string,
  /**
   * @property {bool} spin spin indicates if the icon should be spinning
   */
  spin: _propTypes2.default.bool,
  /**
   * @property {function} onClick callback that the button should execute when clicked
   */
  onClick: _propTypes2.default.func
};

Icon.defaultProps = {
  size: "xs",
  spin: false
};

exports.default = Icon;

//# sourceMappingURL=Icon.js.map