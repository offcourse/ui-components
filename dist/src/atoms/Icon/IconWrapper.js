"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(["\n  box-sizing: border-box;\n"], ["\n  box-sizing: border-box;\n"]);

var _systemComponents = require("system-components");

var _systemComponents2 = _interopRequireDefault(_systemComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var IconWrapper = (0, _systemComponents2.default)({
  is: "div",
  m: 0,
  border: 0,
  fontSize: 1,
  lineHeight: 1,
  bg: "transparent",
  hover: {
    color: "primary"
  },
  focus: {
    outline: "none",
    color: "primary"
  }
}).extend(_templateObject);

exports.default = IconWrapper;

//# sourceMappingURL=IconWrapper.js.map