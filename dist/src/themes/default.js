"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(["\n  body {\n    margin: 0;\n    padding: 0;\n    top: 0;\n    left: 0;\n    right: 0;\n    background: ", ";\n    overflow-x: hidden;\n  }\n\n  @font-face {\n    font-family: 'Nitti Grotesk';\n    src: url('https://app.offcourse.io/fonts/NGN.woff') format('woff');\n  }\n\n  @font-face {\n    font-family: 'Nitti Grotesk Bold';\n    src: url('https://app.offcourse.io/fonts/NGB.woff') format('woff');\n  }\n\n  @font-face {\n    font-family: 'Nitti Bold'; \n    src: url('https://app.offcourse.io/fonts/NB.woff') format('woff');\n  }\n\n  * {\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n  }\n\n  body {\n    font-family: Nitti Grotesk;\n    font-size: 16px;\n    line-height: 20px;\n  }\n"], ["\n  body {\n    margin: 0;\n    padding: 0;\n    top: 0;\n    left: 0;\n    right: 0;\n    background: ", ";\n    overflow-x: hidden;\n  }\n\n  @font-face {\n    font-family: 'Nitti Grotesk';\n    src: url('https://app.offcourse.io/fonts/NGN.woff') format('woff');\n  }\n\n  @font-face {\n    font-family: 'Nitti Grotesk Bold';\n    src: url('https://app.offcourse.io/fonts/NGB.woff') format('woff');\n  }\n\n  @font-face {\n    font-family: 'Nitti Bold'; \n    src: url('https://app.offcourse.io/fonts/NB.woff') format('woff');\n  }\n\n  * {\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n  }\n\n  body {\n    font-family: Nitti Grotesk;\n    font-size: 16px;\n    line-height: 20px;\n  }\n"]);

var _styledComponents = require("styled-components");

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @name Offcourse Theme
 * @description default styles for the Offcourse project
 */

var baseColors = {
  black: "#000000",
  white: "#FFFFFF",
  darkGray: "#3d3d3d",
  mediumGray: "#c0c4c1",
  lightGray: "#f4f6f4",
  yellow: "#E5CF39",
  blue: "#75C7B3",
  green: "#A5CC45",
  red: "#E34D2F"
};

var grayScale = [baseColors.white, baseColors.lightGray, baseColors.mediumGray, baseColors.darkGray, baseColors.black];

var colors = {
  grayScale: grayScale,
  text: baseColors.black,
  white: baseColors.white,
  black: baseColors.black,
  primary: baseColors.blue,
  disabled: grayScale[2],
  positive: baseColors.green,
  warning: baseColors.yellow,
  info: baseColors.blue,
  negative: baseColors.red
};

var fonts = {
  base: "Nitti Grotesk",
  bold: "Nitti Grotesk Bold",
  accent: "Nitti Bold"
};

var breakpoints = ["30rem", "48rem", "64rem"];

var baseUnit = "16px";

var fontSizes = ["0.75rem", "1rem", "1.375rem", "1.75rem"];

var lineHeights = ["1rem", "1.25rem", "1.375rem", "1.75rem", "1.875rem"];

var space = [0, "0.0625rem", "0.125rem", "0.25rem", "0.5rem", "0.75rem", "1rem", "1.5rem", "2rem"];

var borders = [0, "0.0625rem solid", "0.125rem solid"];

var units = {
  sixteenth: "0.0625rem",
  eight: "0.125rem",
  quarter: "0.25rem",
  half: "0.5rem",
  threeQuarter: "0.75rem",
  base: baseUnit,
  baseFont: baseUnit,
  baseLine: "1.25rem",
  logoHeight: "1.875rem",
  subTitleFont: "1.375rem",
  titleFont: "1.75rem",
  titleHeight: "1.875rem"
};

var theme = _defineProperty({
  mode: "default",
  breakpoints: breakpoints,
  fontSizes: fontSizes,
  lineHeights: lineHeights,
  space: space,
  colors: colors,
  borders: borders,
  units: units,
  fonts: fonts,
  grayScale: grayScale
}, "borders", borders);

(0, _styledComponents.injectGlobal)(_templateObject, grayScale[1]);

exports.default = theme;

//# sourceMappingURL=default.js.map