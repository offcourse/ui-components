const path = require("path");
module.exports = {
  type: "react-component",
  webpack: {
    aliases: {
      components: path.resolve("src/components")
    }
  },
  npm: {
    cjs: false,
    esModules: true,
    // umd: {
    //   global: "OffcourseUI",
    //   externals: {
    //     react: "React"
    //   }
    // }
  }
};
