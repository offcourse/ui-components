const path = require("path");
const createNwbWebpackConfig = require("create-nwb-webpack-config");

module.exports = {
  title: "Offcourse UI Components",
  webpackConfig: createNwbWebpackConfig(),
  sections: [
    {
      name: "Introduction",
      content: "docs/introduction.md"
    },
    {
      name: "Theme",
      content: "docs/theme.md"
    },
    {
      name: "Atoms",
      content: "docs/atoms.md",
      components: "src/atoms/**/[A-Z]*.js"
    },
    {
      name: "Molecules",
      components: "src/molecules/**/[A-Z]*.js"
    },
    {
      name: "Organisms",
      components: "src/organisms/**/[A-Z]*.js"
    },
    {
      name: "Utils",
      components: "src/utils/**/[A-Z]*.js"
    }
  ],
  styleguideComponents: {
    Wrapper: path.join(__dirname, "ThemeWrapper")
  },
  pagePerSection: true,
  skipComponentsWithoutExample: true
};
