import ApolloClient from "apollo-boost";

export default new ApolloClient({
  uri: "https://api.offcourse.io/graphql",
  request: async operation => {
    const token = "GUEST";
    operation.setContext({
      headers: {
        authorization: token
      }
    });
  }
});
