import React, { Component } from "react";
import { render } from "react-dom";
import ApolloClient from "apollo-boost";
import App from "./components/App";

const client = new ApolloClient({
  uri: "https://api.offcourse.io/graphql",
  fetchOptions: {
    headers: {
      authorization: "randomToken"
    }
  }
});

render(<App />, document.querySelector("#demo"));
