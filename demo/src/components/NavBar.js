import React, { Component } from "react";
import { Menubar } from "../../../src/organisms";

export default () => {
  const handler = () => alert("HIj");
  const actions = [
    { handler, title: "Create Course" },
    { handler, title: "Profile" },
    { handler, title: "Sign Out" }
  ];
  return (
    <Menubar actions={actions} onLogoClick={handler} onMenuClick={handler} />
  );
};
