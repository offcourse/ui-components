import React, { Component, Fragment } from "react";
import { ApolloProvider } from "react-apollo";
import { ThemeProvider } from "styled-components";
import { CourseCards, Menubar } from "../../../src/organisms";
import client from "../config";
import theme from "../../../src/themes/default";

class App extends React.Component {
  render() {
    const handler = () => alert("HIj");
    const actions = [
      { handler, title: "Create Course" },
      { handler, title: "Profile" },
      { handler, title: "Sign Out" }
    ];
    return (
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <Menubar actions={actions} onLogoClick={handler}>
            <CourseCards cardSize="expanded" />
          </Menubar>
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

export default App;
