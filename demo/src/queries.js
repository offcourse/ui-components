import React, { Component } from "react";
import { Query } from "react-apollo";
import { Loading } from "../../src/atoms";
import { map } from "ramda";
import gql from "graphql-tag";

const courses = gql`
  {
    courses {
      edges {
        node {
          goal
          curator
          timestamp
          courseId
          description
          revision
          tags
          checkpoints {
            task
            checkpointId
            resourceUrl
          }
        }
      }
    }
  }
`;

const byCourseId = gql`
  query Course($courseId: String!) {
    course(courseId: $courseId) {
      goal
      curator
      timestamp
      courseId
      description
      revision
      tags
      checkpoints {
        task
        checkpointId
        resourceUrl
      }
    }
  }
`;

const CoursesQuery = ({ children }) => (
  <Query query={courses}>
    {({ loading, error, data }) => {
      if (loading) return <Loading size="5x" />;
      if (error) return <Debug data={error} />;
      return children({
        courses: map(
          ({ node }) => ({ ...node, courseUrl: "" }),
          data.courses.edges
        )
      });
    }}
  </Query>
);

const CourseQuery = ({ children }) => (
  <Query
    query={byCourseId}
    variables={{ courseId: "3ca438aa-ea68-42e8-bcaf-1926f67496f6" }}
  >
    {({ loading, error, data }) => {
      if (loading) return <Debug data={loading} />;
      if (error) return <Debug data={error} />;
      return children({ course: data.course });
    }}
  </Query>
);

export { CoursesQuery, CourseQuery };
