import { checkpointSchema } from "../schemata";

class Checkpoint {
  static schema = checkpointSchema;
  task = "";
  resourceUrl = "";
}

export default Checkpoint;
