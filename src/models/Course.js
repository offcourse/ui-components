import { courseSchema } from "../schemata";
import Checkpoint from "./Checkpoint";

class Course {
  static schema = courseSchema;
  goal = "";
  checkpoints = [new Checkpoint()];
  description = "";
}

export default Course;
