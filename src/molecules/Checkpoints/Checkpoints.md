```js
const checkpoints = [
  {
    checkpointId: "a",
    task: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag quo`,
    resourceUrl: "/blu"
  },
  {
    checkpointId: "b",
    task: "Do That",
    completed: true,
    resourceUrl: "/blo"
  },
  {
    checkpointId: "c",
    task: "Do More",
    resourceUrl: "/bla"
  }
];

const clickHandler = ({ checkpointId, completed }) =>
  alert(`${checkpointId} is ${completed}`);
<Checkpoints onCheckpointToggle={clickHandler} checkpoints={checkpoints} />;
```
