import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import { map } from "ramda";
import titleCase from "voca/title_case";
import { CheckList } from "../../molecules";

class Checkpoints extends Component {
  renderCheckpoints = () => {
    const { checkpoints } = this.props;
    return map(({ completed, checkpointId, task, resourceUrl }) => {
      return (
        <CheckList.Item
          checkpointId={checkpointId}
          checked={completed}
          url={resourceUrl}
          key={checkpointId}
        >
          {task}
        </CheckList.Item>
      );
    }, checkpoints);
  };

  render() {
    const { onCheckpointToggle, checkpoints } = this.props;
    return (
      <CheckList onCheckboxToggle={onCheckpointToggle}>
        {this.renderCheckpoints()}
      </CheckList>
    );
  }
}

Checkpoints.propTypes = {
  onCheckpointToggle: PropTypes.func,
  checkpoints: PropTypes.arrayOf(
    PropTypes.shape({
      task: PropTypes.string.isRequired,
      resourceUrl: PropTypes.string.isRequired,
      checkpointId: PropTypes.string.isRequired
    })
  ).isRequired
};

export default Checkpoints;
