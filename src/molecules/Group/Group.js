import React, { Component, Children } from "react";
import { addIndex, map } from "ramda";
import PropTypes from "prop-types";
import { Message, List, Link, Button, Tag } from "../../atoms";
import GroupWrapper from "./GroupWrapper";

const mapIndexed = addIndex(map);

class Group extends Component {
  items() {
    const {
      children,
      messages,
      errors,
      basic,
      links,
      buttons,
      tags
    } = this.props;

    if (links) {
      return mapIndexed(
        ({ handler, title }, i) => (
          <List.Item key={i}>
            <Link onClick={handler}>{title}</Link>
          </List.Item>
        ),
        links
      );
    }

    if (tags) {
      return mapIndexed(
        (name, i) => (
          <List.Item key={i}>
            <Tag key={name} href={`/tags/${name}`}>
              {name}
            </Tag>
          </List.Item>
        ),
        tags
      );
    }

    if (buttons) {
      return mapIndexed(
        ({ handler, title, variant }, i) => (
          <List.Item key={i}>
            <Button variant={variant} onClick={handler}>
              {title}
            </Button>
          </List.Item>
        ),
        buttons
      );
    }

    if (errors) {
      return mapIndexed(
        (message, i) => (
          <List.Item key={i}>
            <Message basic={basic} variant="error">
              {message}
            </Message>
          </List.Item>
        ),
        errors
      );
    }
    if (messages) {
      return mapIndexed(
        ({ variant, message }, i) => (
          <List.Item key={i}>
            <Message basic={basic} variant={variant}>
              {message}
            </Message>
          </List.Item>
        ),
        messages
      );
    }

    if (children) {
      return Children.map(children, (child, i) => {
        var newChild = React.cloneElement(child, { basic });
        return <List.Item key={i}>{newChild}</List.Item>;
      });
    }

    return null;
  }

  determineDirectionAndSpacing = () => {
    const {
      buttons,
      tags,
      links,
      messages,
      errors,
      direction,
      spacing
    } = this.props;

    let d, s;

    if (messages || errors) {
      d = "vertical";
      s = "none";
    }

    if (buttons || tags) {
      d = "horizontal";
      s = "tight";
    }

    if (links) {
      d = "horizontal";
      s = "wide";
    }

    return { direction: direction || d, spacing: spacing || s };
  };

  render() {
    const { display, px, pb, pt, justifyContent, flex } = this.props;
    const { direction, spacing } = this.determineDirectionAndSpacing();

    return (
      <GroupWrapper
        display={display}
        pt={pt}
        px={px}
        pb={pb}
        justifyContent={justifyContent}
      >
        <List flex={flex} direction={direction} spacing={spacing}>
          {this.items()}
        </List>
      </GroupWrapper>
    );
  }
}

Group.Action = props => <Link {...props} />;

Group.propTypes = {
  direction: PropTypes.oneOf(["both", "vertical", "horizontal"]),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
};

Group.defaultProps = {};

export default Group;
