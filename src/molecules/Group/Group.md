Vertical

```js
const clickHandler = () => alert("ALERT");
<Group spacing="none" direction="vertical">
  <Link onClick={clickHandler}>Click Me</Link>

  <Link disabled onClick={clickHandler}>
    Cannot Click
  </Link>

  <Link href="/">Follow Me</Link>

  <Link disabled href="/">
    Cannot Follow
  </Link>
</Group>;
```

links as prop

```js
const handler = () => alert("click");
const links = [
  { handler, title: "Create Course" },
  { handler, title: "Profile" },
  { handler, title: "Sign Out" }
];
<Group links={links} />;
```

buttons as prop

```js
const handler = () => alert("click");
const buttons = [
  { handler, title: "Create Course", variant: "primary" },
  { handler, title: "Profile", variant: "positive" },
  { handler, title: "Sign Out", variant: "negative" }
];
<Group buttons={buttons} />;
```

tags as prop

```js
<Group tags={["Kwik", "Kwek", "Kwak"]} />
```

messages as prop

```js
const messages = [
  { variant: "error", message: "error" },
  { variant: "info", message: "info" },
  { variant: "warning", message: "warning" },
  { variant: "success", message: "success" }
];
<Group flex={1} messages={messages} />;
```

errors as prop

```js
const errors = ["This...", "Is....", "Going Down...", "Fast"];
<Group flex={1} basic errors={errors} />;
```
