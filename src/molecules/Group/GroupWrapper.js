import system from "system-components";
import { zIndex } from "styled-system/dist/styles";

const GroupWrapper = system(
  {
    display: "flex",
    flex: 1,
    px: 0,
    pb: 0,
    pt: 0
  },
  "justifyContent"
);

export default GroupWrapper;
