import system from "system-components";

const HandlesWrapper = system({ p: 0 }).extend`
  grid-area: input;
`;

export default HandlesWrapper;
