import React, { Component } from "react";
import PropTypes from "prop-types";
import { map } from "ramda";

class Buttons extends Component {
  static propTypes = {
    actions: PropTypes.array
  };

  prepareButton = ({ title, verb, handler }) => {
    return { title: `${verb} ${title.slice(0, -1)}`, handler };
  };

  render() {
    const { actions } = this.props;
    return (
      <Group
        links={map(this.prepareButton, actions)}
        px={6}
        pt={6}
        justifyContent="flex-end"
      />
    );
  }
}

export default Buttons;
