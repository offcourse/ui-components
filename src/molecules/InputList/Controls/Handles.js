import React, { Component } from "react";
import PropTypes from "prop-types";
import { Icon } from "../../../atoms";
import { Group } from "../../../molecules";
import { Sortable } from "../../../utils";
import HandlesWrapper from "./HandlesWrapper";

const SortHandle = Sortable.Handle(() => (
  <Icon is="button" name="sort" tabIndex="-1" />
));

const RemoveHandle = ({ onRemove }) => (
  <Icon tabIndex="-1" name="remove" onClick={onRemove} />
);

class Handles extends Component {
  render() {
    const { editable, onRemove } = this.props;
    return (
      <HandlesWrapper>
        <Group justifyContent="flex-end" direction="horizontal">
          <RemoveHandle onRemove={onRemove} />
          <SortHandle />
        </Group>
      </HandlesWrapper>
    );
  }
}

export default Handles;
