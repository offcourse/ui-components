import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { map, values, flatten, addIndex } from "ramda";
import { List, Link, Input } from "../../atoms";
import { Buttons, Handles } from "./Controls";

const mapIndexed = addIndex(map);

const ListInput = Input;

class InputList extends Component {
  static propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    items: PropTypes.array,
    errors: PropTypes.arrayOf(
      PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string)
      ])
    ),
    editable: PropTypes.bool,
    onRemove: PropTypes.func,
    onMove: PropTypes.func,
    onAdd: PropTypes.func
  };

  static defaultProps = {
    errors: [],
    items: []
  };

  renderItems() {
    const {
      items,
      editable,
      placeholder,
      name,
      errors,
      FieldComponent,
      onRemove
    } = this.props;
    return mapIndexed((item, index) => {
      return (
        <FieldComponent
          {...item}
          hasErrors={!!errors[index]}
          mb={3}
          placeholder={placeholder}
          onRemove={() => onRemove(index)}
          key={index}
          name={`${name}.${index}`}
        >
          {editable && <Handles onRemove={() => onRemove(index)} />}
        </FieldComponent>
      );
    }, items);
  }

  renderButtons() {
    const { editable, title, onAdd } = this.props;
    const actions = [{ verb: "Add", title, handler: onAdd }];
    return editable && <Buttons actions={actions} />;
  }

  render() {
    const { title, errors, editable, name, onMove } = this.props;
    return (
      <Fragment>
        <List
          useHandle={editable}
          onSortEnd={({ oldIndex, newIndex }) => onMove(oldIndex, newIndex)}
          sortable={editable}
        >
          {this.renderItems()}
        </List>
        {this.renderButtons()}
      </Fragment>
    );
  }
}

export default InputList;
