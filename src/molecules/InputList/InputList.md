Simple Input List

```js
const title = "Form Fields";

<InputList
  placeholder="input"
  title={title}
  name={name}
  items={["", ""]}
  FieldComponent={Input}
/>;
```

Input List with Errors

```js
const title = "Form Fields";

<InputList
  errors={[null, "something went wrong"]}
  placeholder="input"
  title={title}
  name={name}
  items={["", "", ""]}
  FieldComponent={Input}
/>;
```

Sortable Input List

```js
const title = "Form Fields";

const move = (oldIndex, newIndex) =>
  alert(`the items moved from index ${oldIndex} to ${newIndex}`);

const remove = index => alert(`the item with ${index} was removed`);

const add = () => alert(`an item was added`);

<InputList
  placeholder="input"
  title={title}
  name={name}
  items={["", "", ""]}
  FieldComponent={Input}
  editable={true}
  onMove={move}
  onAdd={add}
  onRemove={remove}
/>;
```
