import React, { Component } from "react";
import PropTypes from "prop-types";
import Section from "../../Section";

/**
 * Generic CardSection component for the Offcourse project
 */

class CardSection extends Component {
  render() {
    const { children } = this.props;
    return children ? (
      <Section {...this.props}>{children}</Section>
    ) : (
      <Section {...this.props} />
    );
  }
}

CardSection.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
};

export default CardSection;
