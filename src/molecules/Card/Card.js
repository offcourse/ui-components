import React, { Component } from "react";
import PropTypes from "prop-types";
import { map, addIndex } from "ramda";
import { Expander } from "../../utils";
import CardSection from "./CardSection";
import CardWrapper from "./CardWrapper";

/**
 * @name Card
 * @description Generic Card Component for the Offcourse project
 */

const mapIndexed = addIndex(map);

class Card extends Component {
  cardSections(children) {
    return mapIndexed((section, index) => {
      const { section: sectionName, expand } = section.props;
      return (
        <CardSection
          key={sectionName || index}
          section={sectionName}
          expand={expand}
        >
          {section}
        </CardSection>
      );
    }, children);
  }

  render() {
    const { size, children, hiddenFields, onResize } = this.props;
    return (
      <CardWrapper>
        <Expander size={size} onResize={onResize} hiddenFields={hiddenFields}>
          {this.cardSections(children)}
        </Expander>
      </CardWrapper>
    );
  }
}

Card.Section = CardSection;

Card.propTypes = {
  /**
   * @property {array} child elements
   * */
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  /**
   * @property {object} names of the sections that should be hidden  for each size
   */
  hiddenFields: PropTypes.shape({
    expanded: PropTypes.arrayOf(PropTypes.string),
    medium: PropTypes.arrayOf(PropTypes.string),
    mini: PropTypes.arrayOf(PropTypes.string),
    compact: PropTypes.arrayOf(PropTypes.string)
  }),
  /**
   * @property {string} initial size of the card
   */
  size: PropTypes.string,
  /**
   * @property {function} optional callback that can be triggered on resize
   */
  onResize: PropTypes.func
};

Card.defaultProps = {
  size: "expanded",
  hiddenFields: {
    expanded: []
  },
  onResize: () => {}
};

export default Card;
