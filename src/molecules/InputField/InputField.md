```js
<InputField title="Goal of the Course" name="goal" placeholder="Goal" />
```

```js
<InputField
  variant="small"
  title="Goal of the Course"
  name="goal"
  placeholder="Goal"
/>
```

```js
<InputField
  variant="textarea"
  title="Goal of the Course"
  name="goal"
  placeholder="Goal"
/>
```

```js
<InputField
  errors={["Goal Too Short", "Goal Too Long"]}
  title="Goal of the Course"
  name="goal"
  placeholder="Goal"
/>
```
