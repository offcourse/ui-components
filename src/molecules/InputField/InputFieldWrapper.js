import system from "system-components";
import GroupWrapper from "../Group/GroupWrapper";

const InputFieldWrapper = system({ p: 0 }).extend`
${GroupWrapper} {
  grid-area: input;
}
`;

export default InputFieldWrapper;
