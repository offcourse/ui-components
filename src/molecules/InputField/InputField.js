import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Section, Label, Message, Input } from "../../atoms";
import { isEmpty } from "ramda";
import { Group } from "../../molecules";
import InputFieldWrapper from "./InputFieldWrapper";

export default class InputField extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    touched: PropTypes.array,
    errors: PropTypes.array,
    variant: PropTypes.oneOf(["default", "textarea", "small"])
  };

  static defaultProps = {
    errors: [],
    touched: []
  };

  renderLabel() {
    const { title } = this.props;
    return title ? (
      <Label pb={4} px={6} htmlFor={name}>
        {title}
      </Label>
    ) : null;
  }

  hasErrors() {
    const { errors } = this.props;
    return !isEmpty(errors);
  }

  renderErrors() {
    const { errors } = this.props;
    return this.hasErrors() ? (
      <Group px={6} pb={6} basic errors={errors} />
    ) : null;
  }

  renderChildren() {
    const {
      name,
      placeholder,
      value,
      onChange,
      onBlur,
      type,
      children,
      variant,
      SortHandle,
      RemoveHandle
    } = this.props;

    const fieldProps = {
      name,
      value,
      placeholder,
      onChange,
      onBlur
    };

    return children ? (
      children
    ) : (
      <Input
        {...fieldProps}
        type={type}
        mb={3}
        SortHandle={SortHandle}
        hasErrors={this.hasErrors()}
        RemoveHandle={RemoveHandle}
        variant={variant}
      >
        {RemoveHandle &&
          SortHandle && (
            <Group justifyContent="flex-end" direction="horizontal">
              <RemoveHandle />
              <SortHandle />
            </Group>
          )}
      </Input>
    );
  }

  render() {
    return (
      <InputFieldWrapper>
        {this.renderLabel()}
        {this.renderErrors()}
        {this.renderChildren()}
      </InputFieldWrapper>
    );
  }
}
