import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import titleCase from "voca/title_case";
import Checkbox from "../../../atoms/Checkbox";
import CheckListItemWrapper from "./CheckListItemWrapper";

/**
 * List Item component for the Offcourse project
 *
 * @version 0.1.0
 * @author [Jan Hein Hoogstad](https://gitlab.com/yeehaa)
 */

class CheckListItem extends Component {
  handleCheckboxToggle = ({ checked }) => {
    const { onCheckboxToggle, index, ...rest } = this.props;
    onCheckboxToggle({
      ...rest,
      index,
      completed: checked
    });
  };

  render() {
    const { children, checked, url, onCheckboxToggle, mb, mr } = this.props;
    return (
      <CheckListItemWrapper
        is={url ? "a" : "li"}
        mb={mb}
        mr={mr}
        gridTemplateColumns={onCheckboxToggle ? "2rem 1fr" : "1fr"}
        href={url}
      >
        {onCheckboxToggle && (
          <Checkbox onToggle={this.handleCheckboxToggle} checked={checked} />
        )}
        {titleCase(children)}
      </CheckListItemWrapper>
    );
  }
}

CheckListItem.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
    .isRequired,
  index: PropTypes.number,
  checked: PropTypes.bool,
  onCheckboxToggle: PropTypes.func
};

CheckListItem.defaultProps = {
  variant: "default"
};

export default CheckListItem;
