import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import { addIndex, map } from "ramda";
import CheckListItem from "./CheckListItem";
import { List } from "../../atoms";

const mapIndexed = addIndex(map);

class CheckList extends Component {
  renderItems = () => {
    const { items } = this.props;
    return mapIndexed(
      ({ title, checked }, index) => (
        <CheckListItem key={index} checked={checked}>
          {title}
        </CheckListItem>
      ),
      items
    );
  };

  render() {
    const { items, ...rest } = this.props;
    if (items) {
      return <List {...rest}>{this.renderItems()}</List>;
    }
    return <List {...this.props} />;
  }
}

CheckList.Item = CheckListItem;

CheckList.propTypes = {
  /** the different elements that the list consists of */
  children: PropTypes.arrayOf(PropTypes.element),
  /** field that indicates how much spacing a list needs */
  spacing: PropTypes.oneOf(["none", "tight", "normal", "wide"]),
  /** field that indicates the direction of the list */
  direction: PropTypes.oneOf(["horizontal", "vertical"]),
  /** checkbox items that can be rendered all at once */
  items: PropTypes.arrayOf(
    PropTypes.shape({
      checked: PropTypes.bool.isRequired,
      title: PropTypes.string.isRequired
    })
  ),
  onCheckboxToggle: PropTypes.func
};

CheckList.defaultProps = {
  spacing: "tight",
  direction: "vertical"
};

export default CheckList;
