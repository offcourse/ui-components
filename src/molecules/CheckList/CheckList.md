Vertical CheckList

```js
<CheckList
  onCheckboxToggle={({ index, checked }) =>
    alert(
      `the index of this item is: ${index}, its checked status is ${checked}`
    )
  }
>
  <CheckList.Item checked={true}>Kwik</CheckList.Item>
  <CheckList.Item>Kwek</CheckList.Item>
  <CheckList.Item>Kwak</CheckList.Item>
</CheckList>
```

Horizontal CheckList

```js
<CheckList
  direction="horizontal"
  spacing="normal"
  onCheckboxToggle={({ index, checked }) =>
    alert(
      `the index of this item is: ${index}, its checked status is ${checked}`
    )
  }
>
  <CheckList.Item checked={true}>Kwik</CheckList.Item>
  <CheckList.Item>Kwek</CheckList.Item>
  <CheckList.Item>Kwak</CheckList.Item>
</CheckList>
```

items as props

```js
const items = [
  { title: "kwik", checked: true },
  { title: "kwek", checked: false },
  { title: "kwak", checked: false }
];
<CheckList
  items={items}
  onCheckboxToggle={({ index, checked }) =>
    alert(
      `the index of this item is: ${index}, its checked status is ${checked}`
    )
  }
/>;
```
