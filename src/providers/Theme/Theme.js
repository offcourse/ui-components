import React, { Component } from "react";
import PropTypes from "prop-types";
import { ThemeProvider as Provider } from "styled-components";
import defaultTheme from "./default";

/**
 * @name ThemeProvider
 * @description Top level component that applies styling to all its children
 * and further descendants
 */

export default class ThemeProvider extends Component {
  static propTypes = {
    /**
     * @property {object} theme style properties that all components share
     */
    theme: PropTypes.object
  };

  static defaultProps = {
    theme: defaultTheme
  };

  render() {
    const { theme, ...rest } = this.props;
    return <ThemeProvider theme={theme} />;
  }
}
