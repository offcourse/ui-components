import yup from "yup";

const checkpoint = yup.object().shape({
  task: yup
    .string()
    .min(5)
    .max(55)
    .required(),
  resourceUrl: yup
    .string()
    .url()
    .required()
});

export default checkpoint;
