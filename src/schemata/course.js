import yup from "yup";
import checkpoint from "./checkpoint";

const course = yup.object().shape({
  goal: yup
    .string()
    .min(5)
    .max(55)
    .required(),
  checkpoints: yup
    .array()
    .of(checkpoint)
    .min(2)
    .max(7)
    .required(),
  description: yup
    .string()
    .min(10)
    .max(210)
});

export default course;
