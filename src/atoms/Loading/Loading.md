```js
<div>
  <Loading size="xs" />
  <Loading size="sm" />
  <Loading size="lg" />
  <Loading size="2x" />
  <Loading size="3x" />
  <Loading size="4x" />
  <Loading size="5x" />
  <Loading size="6x" />
  <Loading size="8x" />
  <Loading size="9x" />
  <Loading size="10x" />
</div>
```
