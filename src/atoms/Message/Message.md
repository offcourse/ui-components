```js
<Group flex={1} direction="vertical" spacing="none">
  <Message>This is a message</Message>
  <Message variant="error">This is an error</Message>
  <Message variant="info">This is an info</Message>
  <Message variant="warning">This is a warning</Message>
  <Message variant="success">This is a success</Message>
</Group>
```

```js
<Group flex={1} spacing="none" direction="vertical" basic>
  <Message>This is a message</Message>
  <Message variant="error">This is an error</Message>
  <Message variant="info">This is an info</Message>
  <Message variant="warning">This is a warning</Message>
  <Message variant="success">This is a success</Message>
</Group>
```
