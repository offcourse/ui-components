import React, { Component } from "react";
import PropTypes from "prop-types";
import { keys } from "ramda";
import I from "react-icons-kit";
import { check } from "react-icons-kit/fa/check";
import { asterisk } from "react-icons-kit/fa/asterisk";
import { bars } from "react-icons-kit/fa/bars";
import { times } from "react-icons-kit/fa/times";
import { plus } from "react-icons-kit/fa/plus";
import { sort } from "react-icons-kit/fa/sort";
import { eye } from "react-icons-kit/fa/eye";
import { facebook } from "react-icons-kit/fa/facebook";
import { twitter } from "react-icons-kit/fa/twitter";
import IconWrapper from "./IconWrapper";

const icons = {
  facebook,
  twitter,
  asterisk,
  sort,
  eye: eye,
  remove: times,
  add: plus,
  hamburger: Bars
};

/**
 * @name Icon
 * @description Icon Component
 */

class Icon extends Component {
  icon() {
    const { name } = this.props;
    return icons[name];
  }

  render() {
    const { size, spin, tabIndex, name, href, is, onClick } = this.props;
    return (
      <IconWrapper
        is={is || (href && "a") || (onClick && "button")}
        type={(is === "button" && "button") || (onClick && "button")}
        tabIndex={tabIndex}
        onClick={onClick}
      >

        <I icon={this.icon()} size={size} spin={false} />
      </IconWrapper>
    );
  }
}

const iconNames = keys(icons);

Icon.propTypes = {
  /**
   * @property {string} name name of the icon
   */
  name: PropTypes.oneOf(iconNames),
  /**
   * @property {string} size size of the icon
   */
  size: PropTypes.oneOf([
    "xs",
    "sm",
    "lg",
    "2x",
    "3x",
    "4x",
    "5x",
    "6x",
    "7x",
    "8x",
    "9x",
    "10x"
  ]),
  /**
   * @property {string} href url that the button should link to, automatically turns the button into the basic type
   */
  href: PropTypes.string,
  /**
   * @property {bool} spin spin indicates if the icon should be spinning
   */
  spin: PropTypes.bool,
  /**
   * @property {function} onClick callback that the button should execute when clicked
   */
  onClick: PropTypes.func
};

Icon.defaultProps = {
  size: "xs",
  spin: false
};

export default Icon;
