import system from "system-components";
import { theme as t } from "styled-system";

const background = {
  default: "grayScale.3",
  disabled: "disabled",
  primary: "primary",
  positive: "positive",
  negative: "negative"
};

const BaseButton = system(
  {
    is: "button",
    m: 0,
    border: 0,
    fontSize: 1,
    lineHeight: 1,
    borderColor: "primary",
    focus: { outline: "none" }
  },
  props => {
    const { variant, theme } = props;

    return {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: variant && t(background[variant])(props),
      fontFamily: theme.fonts.bold,
      "&:disabled": { cursor: "default" },
      "> a": {
        color: "inherit",
        textDecoration: "inherit"
      },
      boxSizing: "border-box"
    };
  }
);

export default BaseButton;
