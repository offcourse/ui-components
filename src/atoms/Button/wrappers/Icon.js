import styled from "styled-components";
import { borders, color } from "styled-system";
import BaseButton from "./Base";

const attrs = {
  border: 0,
  bg: "white"
};

const IconWrapper = BaseButton.extend.attrs(attrs)`
  ${borders};
  ${color};
  vertical-align: -0.25em;
`;

export default IconWrapper;
