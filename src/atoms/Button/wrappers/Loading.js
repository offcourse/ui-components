import styled from "styled-components";
import { color, width } from "styled-system";
import ButtonWrapper from "./Button";

const attrs = {
  color: "grayScale.1",
  width: 120
};
const LoadingWrapper = ButtonWrapper.extend.attrs(attrs)`
  ${color};
  ${width};
  vertical-align: -0.45em;
`;

export default LoadingWrapper;
