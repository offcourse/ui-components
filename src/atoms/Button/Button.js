import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { ButtonWrapper, LoadingWrapper, IconWrapper } from "./wrappers";
import Loading from "../Loading";
import Icon from "../Icon";
import styled from "styled-components";

/**
 * The button component for the Offcourse project
 */

class Button extends Component {
  handleClick = e => {
    const { onClick } = this.props;
    e.preventDefault();
    onClick();
  };

  render() {
    const {
      href,
      icon,
      type,
      children,
      loading,
      onClick,
      tabindex,
      variant,
      ...rest
    } = this.props;
    const buttonType = type || "button";

    const disabled = this.props.disabled || this.props.loading;

    if (icon) {
      return (
        <ButtonWrapper
          type={buttonType}
          {...rest}
          onClick={this.handleClick}
          tabIndex={tabindex || 1}
        >
          <Icon size="lg" name={icon} />
        </ButtonWrapper>
      );
    }

    if (loading) {
      return (
        <LoadingWrapper
          variant={disabled ? "disabled" : variant}
          disabled={disabled}
        >
          <Loading />
        </LoadingWrapper>
      );
    }

    return (
      <ButtonWrapper
        type={buttonType}
        variant={disabled ? "disabled" : variant}
        disabled={disabled}
        onClick={onClick}
        tabIndex={tabindex || 1}
      >
        {href ? (
          <a href={!disabled ? href : undefined}>{children}</a>
        ) : (
          children
        )}
      </ButtonWrapper>
    );
  }
}

Button.propTypes = {
  /** determines if the button should be disabled */
  disabled: PropTypes.bool,
  /** determines the status of the button */
  variant: PropTypes.oneOf(["default", "primary", "positive", "negative"]),
  /** determines if a button has a loading status */
  loading: PropTypes.bool,
  /** the text that is displayed on the button */
  children: PropTypes.string,
  /** code that the button should execute when clicked */
  onClick: PropTypes.func,
  /** decides whether an icon should be displayed, and if yes, the name of the icon */
  icon: PropTypes.string,
  /** a url that the button should link to, automatically turns the button into the basic type */
  href: PropTypes.string,
  /** only accept a type prop when type is submit */
  type: PropTypes.oneOf(["submit", "button"])
};

Button.defaultProps = {
  variant: "default",
  disabled: false,
  type: "button"
};

export default Button;
