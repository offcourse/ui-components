Default, Disabled, and Loading Button:

```js
const clickHandler = () => alert("ALERT");
<Group spacing="tight" direction="horizontal">
  <Button onClick={clickHandler}>Click Me</Button>
  <Button onClick={clickHandler} disabled>
    Cannot Click
  </Button>
  <Button loading onClick={clickHandler}>
    Click Later
  </Button>
</Group>;
```

Primary, Positive, and Negative Button:

```js
const clickHandler = () => alert("ALERT");
<Group spacing="tight" direction="horizontal">
  <Button variant="primary" href="/">
    Click Now!
  </Button>
  <Button variant="positive" onClick={clickHandler}>
    Please Click
  </Button>
  <Button variant="negative" onClick={clickHandler}>
    Don't Click
  </Button>
</Group>;
```

Icon Button can take an onClick or href:

```js
const clickHandler = () => alert("ALERT");
<Group spacing="tight" direction="horizontal">
  <Button icon="facebook" onClick={clickHandler}>
    Click Me
  </Button>
</Group>;
```
