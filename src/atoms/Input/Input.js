import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import titleCase from "voca/title_case";
import Icon from "../../atoms/Icon";
import { InputWrapper, OuterWrapper, TextAreaWrapper } from "./wrappers";

class Input extends Component {
  render() {
    const {
      mb,
      pt,
      pb,
      variant,
      type,
      children,
      hasErrors,
      name,
      value,
      placeholder,
      onChange,
      onBlur
    } = this.props;
    const small = variant === "small";
    const formattedPlaceholder = titleCase(placeholder);

    const fieldProps = {
      name,
      value,
      placeholder: formattedPlaceholder,
      onChange,
      onBlur
    };

    return (
      <OuterWrapper border={hasErrors ? 2 : 0} pb={pb} pt={pt} mb={mb}>
        {children && children}
        {variant === "textarea" ? (
          <TextAreaWrapper is="textarea" rows="4" {...fieldProps} />
        ) : (
          <InputWrapper
            type={type || "text"}
            fontSize={small ? 1 : 3}
            lineHeight={small ? 1 : 3}
            {...fieldProps}
          />
        )}
      </OuterWrapper>
    );
  }
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  hasErrors: PropTypes.bool,
  variant: PropTypes.oneOf(["default", "textarea", "small"])
};

Input.defaultProps = {
  variant: "default"
};

export default Input;
