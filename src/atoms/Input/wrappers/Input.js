import styled from "styled-components";
import BaseWrapper from "./Base";

const InputWrapper = BaseWrapper.extend`
  grid-area: input;
`;

export default InputWrapper;
