```js
<Input name="test" placeholder="Test" />
```

```js
<Input name="test" hasErrors placeholder="Test" />
```

```js
<Input name="test" variant="small" placeholder="Test" />
```

```js
<Input name="test" variant="textarea" placeholder="Test" />
```
