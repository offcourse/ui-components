import React, { Component } from "react";
import SwatchWrapper from "./SwatchWrapper";

class Swatch extends Component {
  render() {
    const { children, color } = this.props;
    return <SwatchWrapper bg={color}>{color}</SwatchWrapper>;
  }
}

export default Swatch;
