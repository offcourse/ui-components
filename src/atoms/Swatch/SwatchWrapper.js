import system from "system-components";

const SwatchWrapper = system(
  {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    m: 0,
    height: "3rem",
    bg: "black",
    borderColor: "black",
    border: 1
  },
  props => ({
    fontFamily: props.theme.fonts.bold,
    borderColor: "black",
    color:
      props.bg === "grayScale.4" || props.bg === "grayScale.3"
        ? "white"
        : "black"
  })
);

export default SwatchWrapper;
