## Color Palette

```js
<Swatch color="primary" />
<Swatch color="disabled" />
<Swatch color="positive" />
<Swatch color="warning" />
<Swatch color="info" />
<Swatch color="negative" />
```

## Grayscale

```js
<Swatch color="grayScale.0" />
<Swatch color="grayScale.1" />
<Swatch color="grayScale.2" />
<Swatch color="grayScale.3" />
<Swatch color="grayScale.4" />
```
