```js
<Group flex={1} spacing="tight" direction="both">
  <Tag>Spicy</Tag>
  <Tag>Jalapeno</Tag>
  <Tag>Tri-Tip</Tag>
  <Tag>Jowl</Tag>
  <Tag>Prosciutto</Tag>
  <Tag>Short Ribs</Tag>
  <Tag>Pork</Tag>
  <Tag>Ribeye</Tag>
  <Tag>Tenderloin</Tag>
  <Tag>Porchetta</Tag>
  <Tag>Landjaeger</Tag>
  <Tag>Fatback</Tag>
</Group>
```
