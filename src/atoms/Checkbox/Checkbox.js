import React, { Component } from "react";
import PropTypes from "prop-types";
import Icon from "react-icons-kit";
import { check } from "react-icons-kit/fa/check";
import { OuterWrapper, LabelWrapper } from "./wrappers";

/**
 * @name Checkbox
 * @description Checkbox component for the offcourse project
 * @author Yeehaa
 */

class Checkbox extends Component {
  state = {
    checked: this.props.checked
  };

  handleClick = e => {
    e.preventDefault();
    const { onToggle } = this.props;
    this.setState(
      ({ checked }) => ({ checked: !checked }),
      () => onToggle(this.state)
    );
  };

  render() {
    const { checked } = this.state;
    return (
      <OuterWrapper onClick={this.handleClick}>
        <input type="checkbox" readOnly checked={checked} />
        <LabelWrapper>
          <Icon icon={check} />
        </LabelWrapper>
      </OuterWrapper>
    );
  }
}

Checkbox.propTypes = {
  /**
   * @property {bool} checked allows the user to directly decide if the value is checked or not
   */
  checked: PropTypes.bool,
  /**
   * @property {function} ontoggle is an optional callback that triggers when the checkbox changes its value
   */
  onToggle: PropTypes.func
};

Checkbox.defaultProps = {
  checked: false,
  onToggle: () => null
};

export default Checkbox;
