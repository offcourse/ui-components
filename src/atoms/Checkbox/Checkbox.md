```js
const onToggle = ({ checked }) =>
  alert(`the checkbox is now ${checked ? "" : "un"}checked`);

<Background>
  <div
    style={{
      display: "grid",
      gridGap: "0.5rem"
    }}
  >
    <Checkbox onToggle={onToggle} checked={false} />
    <Checkbox onToggle={onToggle} checked={true} />
  </div>
</Background>;
```
