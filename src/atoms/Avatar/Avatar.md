Avatar with Custom Image

```js
const url = "https://assets.offcourse.io/portraits/offcourse_1.jpg";
const name = "Yeehaa";
<Avatar url={url} name={name} />;
```

Avatar with Default Image

```js
const name = "Yeehaa";
<Avatar name={name} />;
```
