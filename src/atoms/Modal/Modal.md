```js
initialState = { isOpen: false };
<div id="test">
  <Button onClick={() => setState({ isOpen: true })}>Open Modal</Button>
  <Modal isOpen={state.isOpen}>
    <Section>
      <Header>This is a Modal</Header>
      <Group justifyContent="flex-end">
        <Button onClick={() => setState({ isOpen: false })}>Close Modal</Button>
      </Group>
    </Section>
  </Modal>
</div>;
```
