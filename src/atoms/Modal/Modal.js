import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactModal from "styled-modal";
import Container from "./Container";
import Overlay from "./Overlay";

class Modal extends Component {
  render() {
    const { isOpen, children } = this.props;
    return isOpen ? (
      <ReactModal containerComponent={Overlay} modalComponent={Container}>
        <div className="content">{children}</div>
      </ReactModal>
    ) : null;
  }
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
};

export default Modal;
