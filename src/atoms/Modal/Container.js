import React, { Component } from "react";
import system from "system-components";

const Overlay = system({
  bg: "grayScale.4",
  position: "fixed",
  zIndex: 999,
  top: 0,
  right: 0,
  bottom: 0,
  left: 0
}).extend`
  opacity: 0.9;
`;

const ContainerWrapper = system({
  fontSize: 1,
  minWidth: "20rem",
  maxWidth: "40rem",
  width: "40rem",
  bg: "white",
  m: 4
});
class Container extends Component {
  render() {
    return <ContainerWrapper>{this.props.children}</ContainerWrapper>;
  }
}

export default Container;
