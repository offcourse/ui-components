import system from "system-components";

const Overlay = system({
  bg: "grayScale.4",
  position: "fixed",
  zIndex: 999,
  top: 0,
  right: 0,
  bottom: 0,
  left: 0
}).extend`
  opacity: 0.9;
`;

export default Overlay;
