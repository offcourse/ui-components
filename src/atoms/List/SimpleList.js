import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import ListWrapper from "./ListWrapper";

class SimpleList extends Component {
  renderItems = () => {
    const { children, ...rest } = this.props;
    return Children.map(children, (child, index) => {
      return React.cloneElement(child, {
        index,
        ...rest
      });
    });
  };

  render() {
    const { spacing, flex, direction } = this.props;
    return (
      <ListWrapper
        flex={flex}
        spacing={spacing}
        direction={direction}
        flexDirection={
          direction === "horizontal" || direction === "both" ? "row" : "column"
        }
      >
        {this.renderItems()}
      </ListWrapper>
    );
  }
}

export default SimpleList;
