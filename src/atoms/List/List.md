Vertical List with Items:

```js
<List>
  <List.Item>Kwik</List.Item>
  <List.Item>Kwek</List.Item>
  <List.Item>Kwak</List.Item>
</List>
```

Horizontal List with Items:

```js
<List direction="horizontal">
  <List.Item>Kwik</List.Item>
  <List.Item>Kwek</List.Item>
  <List.Item>Kwak</List.Item>
</List>
```

List can have extra spacing:

```js
<List direction="horizontal" spacing="wide">
  <List.Item>Kwik</List.Item>
  <List.Item>Kwek</List.Item>
  <List.Item>Kwak</List.Item>
</List>
```

...less spacing:

```js
<List direction="horizontal" spacing="tight">
  <List.Item>Kwik</List.Item>
  <List.Item>Kwek</List.Item>
  <List.Item>Kwak</List.Item>
</List>
```

or no spacing:

```js
<List direction="horizontal" spacing="none">
  <List.Item>Kwik</List.Item>
  <List.Item>Kwek</List.Item>
  <List.Item>Kwak</List.Item>
</List>
```

Lists can be sortable...

```js
initialState = {
  items: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"]
};
const onSortEnd = ({ oldIndex, newIndex }) => {
  setState({
    items: Sortable.move(state.items, oldIndex, newIndex)
  });
};
<List direction="horizontal" spacing="wide" sortable onSortEnd={onSortEnd}>
  {state.items.map((item, index) => <List.Item key={index}>{item}</List.Item>)}
</List>;
```

...with or without an handle

```js
initialState = {
  items: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"]
};

const SortHandle = <Icon is="button" name="sort" tabindex="-1" />;

const onSortEnd = ({ oldIndex, newIndex }) => {
  setState({
    items: Sortable.move(state.items, oldIndex, newIndex)
  });
};

<List sortable SortHandle={SortHandle} onSortEnd={onSortEnd}>
  {state.items.map((item, index) => <List.Item key={index}>{item}</List.Item>)}
</List>;
```

...with or without an handle

```js
initialState = {
  items: [
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
    "Item 7",
    "Item 8",
    "Item 9"
  ]
};

const SortHandle = <Icon is="button" name="sort" tabindex="-1" />;

const onSortEnd = ({ oldIndex, newIndex }) => {
  setState({
    items: Sortable.move(state.items, oldIndex, newIndex)
  });
};
<List sortable direction="both" SortHandle={SortHandle} onSortEnd={onSortEnd}>
  {state.items.map((item, index) => <List.Item key={index}>{item}</List.Item>)}
</List>;
```
