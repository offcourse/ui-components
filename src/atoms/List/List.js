import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import ListItem from "./ListItem";
import ListWrapper from "./ListWrapper";
import { Sortable } from "../../utils";
import SimpleList from "./SimpleList";
import SortableList from "./SortableList";

/**
 * Generic list component for the Offcourse project
 */

const directionTable = {
  horizontal: "x",
  vertical: "y",
  both: "xy"
};

class List extends Component {
  static Item = ListItem;

  static propTypes = {
    /** the different elements that the list consists of */
    children: PropTypes.arrayOf(PropTypes.element),
    /** field that indicates how much spacing a list needs */
    spacing: PropTypes.oneOf(["none", "tight", "normal", "wide"]),
    /** field that indicates the direction of the list */
    direction: PropTypes.oneOf(["horizontal", "vertical", "both"]),
    /** flag that indicates if the list should be sortable */
    sortable: PropTypes.bool
  };

  static defaultProps = {
    spacing: "normal",
    direction: "vertical",
    horizontal: false,
    sortable: false
  };

  render() {
    const {
      children,
      flex,
      sortable,
      onSortEnd,
      SortHandle,
      useHandle,
      direction,
      spacing,
      ...rest
    } = this.props;
    return sortable ? (
      <Sortable
        SortHandle={SortHandle && Sortable.Handle(() => SortHandle)}
        flex={flex}
        onSortEnd={onSortEnd}
        axis={directionTable[direction]}
        direction={direction}
        spacing={spacing}
        Component={SortableList}
        useDragHandle={!!SortHandle || useHandle}
      >
        {children}
      </Sortable>
    ) : (
      <SimpleList flex={flex} direction={direction} spacing={spacing} {...rest}>
        {children}
      </SimpleList>
    );
  }
}

export default List;
