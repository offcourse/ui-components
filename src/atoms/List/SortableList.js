import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import ListWrapper from "./ListWrapper";
import ListItem from "./ListItem";

/**
 * Generic list component for the Offcourse project
 */

class SortableList extends Component {
  renderItems = () => {
    const { children, SortHandle } = this.props;
    return Children.map(children, (child, index) => {
      return React.cloneElement(child, {
        children: Children.map(child.props.children, child => {
          if (child.type === ListItem) {
            return React.cloneElement(child, {
              index,
              SortHandle,
              sortable: true
            });
          } else {
            return child;
          }
        })
      });
    });
  };

  render() {
    const { direction, flex, spacing } = this.props;
    return (
      <ListWrapper
        flex={flex}
        flexDirection={
          direction === "horizontal" || direction === "both" ? "row" : "column"
        }
        direction={direction}
        spacing={spacing}
      >
        {this.renderItems()}
      </ListWrapper>
    );
  }
}

export default SortableList;
