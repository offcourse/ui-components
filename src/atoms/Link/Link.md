Link can take an onClick or href:

```js
const clickHandler = () => alert("ALERT");
<Group>
  <Link onClick={clickHandler}>Click Me</Link>

  <Link disabled onClick={clickHandler}>
    Cannot Click
  </Link>

  <Link href="/">Follow Me</Link>

  <Link disabled href="/">
    Cannot Follow
  </Link>
</Group>;
```
