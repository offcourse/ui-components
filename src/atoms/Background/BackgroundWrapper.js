import system from "system-components";

const BackgroundWrapper = system({
  display: "flex",
  flexWrap: "wrap",
  flexDirection: "row",
  p: 6,
  m: 0,
  bg: 3
}).extend`
`;

export default BackgroundWrapper;
