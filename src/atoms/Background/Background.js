import React, { Component } from "react";
import BackgroundWrapper from "./BackgroundWrapper";

class Background extends Component {
  render() {
    const { children, dark, p } = this.props;
    const bg = `grayScale.${dark ? 3 : 1}`;
    return (
      <BackgroundWrapper p={p} bg={bg}>
        {children}
      </BackgroundWrapper>
    );
  }
}

Background.defaultProps = {
  dark: false
};

export default Background;
