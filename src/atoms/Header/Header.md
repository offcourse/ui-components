Header can have a normal size

```js
<Header>Regular Header</Header>
```

They can be small

```js
<Header size="small">Small Header</Header>
```

They can link

```js
<Header href="/">Regular Header</Header>
```
