```js
const handler = message => alert(JSON.stringify(message, null, 2));

<div style={{ backgroundColor: "#f4f6f4", padding: "1em", margin: "-1rem" }}>
  <Auth signIn={handler} signUp={handler} resetPassword={handler} />
</div>;
```
