import React from "react";
import { shallow } from "enzyme";
import SignUpForm from "./SignUpForm";

describe("Sign Up Form", () => {
  const handler = jest.fn();

  describe("normal mode", () => {
    const userName = "yeehaa";
    const wrapper = shallow(
      <SignUpForm
        userName={userName}
        onRequestSignIn={handler}
        onSubmit={handler}
      />
    );
    it("sets mode to normal", () => {
      const el = wrapper.props().children({ errors: {} });
      expect(el.props.mode).toBe("normal");
    });

    it("calls the handler with the username", () => {
      wrapper.instance().requestSignIn();
      expect(handler).toBeCalledWith({ userName });
    });
  });

  describe("confirm mode", () => {
    const wrapper = shallow(
      <SignUpForm confirmMode={true} onSubmit={handler} />
    );
    it("sets the mode to confirm", () => {
      const el = wrapper.props().children({ errors: {} });
      expect(el.props.mode).toBe("confirm");
    });
  });
});
