import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import Form from "../Form";
import schema from "./signUpSchema";

const blankProfile = {
  userName: "",
  email: "",
  password: "",
  confirmationCode: ""
};

class SignUpForm extends Component {
  render() {
    const {
      confirmMode,
      userName,
      onSubmit,
      linkData,
      errors,
      onCancel
    } = this.props;

    const mode = confirmMode ? "confirm" : "normal";
    return (
      <Form
        initialValues={userName ? { ...blankProfile, userName } : blankProfile}
        linkData={linkData}
        mode={mode}
        errors={errors}
        schema={schema}
        title="Sign Up"
        onSubmit={onSubmit}
        onCancel={onCancel}
        setSubmitting={!confirmMode}
      >
        {({ errors }) => {
          return (
            <Fragment>
              <Form.Field
                title="User Name"
                name="userName"
                placeholder="User Name"
              />
              <Form.Field
                title="Email"
                type="email"
                name="email"
                placeholder="Email Address"
              />
              <Form.Field
                title="Password"
                type="password"
                name="password"
                placeholder="Password"
              />

              {confirmMode && (
                <Form.Field
                  title="Confirmation Code"
                  name="confirmationCode"
                  placeholder="Confirmation Code"
                />
              )}
            </Fragment>
          );
        }}
      </Form>
    );
  }
}

SignUpForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onRequestSignIn: PropTypes.func,
  confirmMode: PropTypes.bool,
  userName: PropTypes.string,
  errors: PropTypes.shape({
    userName: PropTypes.string,
    email: PropTypes.string,
    confirmationCode: PropTypes.string,
    password: PropTypes.string
  })
};

export default SignUpForm;
