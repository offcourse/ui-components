```js
const confirmMode = false;
const errors = {};
const userName = "";
const handler = () => alert("Done!");

<Background>
  <SignUpForm
    confirmMode={confirmMode}
    errors={errors}
    userName={userName}
    onSubmit={handler}
    onRequestSignIn={handler}
  />
</Background>;
```

```js
const confirmMode = true;
const errors = {};
const userName = "";
const handler = () => alert("Done!");

<Background>
  <SignUpForm
    confirmMode={confirmMode}
    errors={errors}
    userName={userName}
    onSubmit={handler}
    onRequestSignIn={handler}
  />
</Background>;
```
