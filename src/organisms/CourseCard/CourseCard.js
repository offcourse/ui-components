import React, { Component } from "react";
import PropTypes from "prop-types";
import { isEmpty, isNil } from "ramda";
import { Tag, Header, Card } from "../../atoms";
import { CourseProvider } from "../../utils";
import {
  Description,
  Group,
  Share,
  Curator,
  Checkpoints
} from "../../molecules";

/**
 * The course card component for the Offcourse project
 */

class CourseCard extends Component {
  state = {
    course: this.props.course
  };

  static propTypes = {
    size: PropTypes.oneOf(["expanded", "medium", "mini", "compact"]),
    onCheckpointToggle: PropTypes.func,
    onResize: PropTypes.func,
    hiddenFields: PropTypes.object,
    shareMessage: PropTypes.string,
    course: PropTypes.shape({
      courseId: PropTypes.string.isRequired,
      goal: PropTypes.string.isRequired,
      curator: PropTypes.string.isRequired,
      avatarUrl: PropTypes.string,
      courseUrl: PropTypes.string
    })
  };

  static defaultProps = {
    onResize: () => {},
    hiddenFields: {
      expanded: [],
      medium: ["checkpoints"],
      mini: ["meta", "checkpoints", "tags"],
      compact: ["meta", "checkpoints", "tags", "description"]
    }
  };

  handleCheckpointToggle = ({ index, completed }) => {
    const { course } = this.state;
    const { onCheckpointToggle } = this.props;
    onCheckpointToggle({
      course,
      checkpointId: course.checkpoints[index].checkpointId,
      complete: completed
    });
  };

  hasTags = tags => {
    return !isEmpty(tags) && !isNil(tags);
  };

  renderCard = course => {
    const {
      shareMessage,
      size,
      onResize,
      onCheckpointToggle,
      hiddenFields
    } = this.props;
    const {
      courseId,
      goal,
      curator,
      courseUrl,
      avatarUrl,
      profileUrl,
      checkpoints,
      description,
      tags
    } = course;
    return (
      <Card
        size={size}
        onResize={({ size }) => onResize({ size, courseId })}
        hiddenFields={hiddenFields}
      >
        <Header section="header" expand="onDoubleClick">
          {goal}
        </Header>
        <Curator
          section="meta"
          name={curator}
          profileUrl={profileUrl}
          avatarUrl={avatarUrl}
        />
        {description && (
          <Description label="Course Description" section="description">
            {description}
          </Description>
        )}
        <Checkpoints
          section="checkpoints"
          onCheckpointToggle={onCheckpointToggle && this.handleCheckpointToggle}
          checkpoints={checkpoints}
        />
        {this.hasTags(tags) && (
          <Group flex={1} direction="both" section="tags" tags={tags} />
        )}
        <Share
          section="social"
          url={courseUrl || "loading..."}
          text={shareMessage || "Checkout this Course!"}
          providers={["twitter", "facebook", "url"]}
        />
      </Card>
    );
  };

  render() {
    const { course, goal, curator, courseId } = this.props;
    const courseQuery = goal && curator && { goal, curator };
    if (course) {
      return this.renderCard(course);
    }
    return (
      <CourseProvider courseQuery={courseQuery} courseId={courseId}>
        {({ course }) => {
          return this.renderCard(course);
        }}
      </CourseProvider>
    );
  }
}

export default CourseCard;
