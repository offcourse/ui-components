CourseCard Component in different sizes. Double Click Goal to Expand.

```js
const course = {
  courseId: "abc",
  goal: "Learn This",
  curator: "Offcourse",
  courseUrl: "/yeehaa",
  profileUrl: `/curator/yeehaa`,
  checkpoints: [
    {
      checkpointId: "a",
      task: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag quo`,
      resourceUrl: "/"
    },
    {
      checkpointId: "b",
      task: "Do That",
      completed: true,
      resourceUrl: "/"
    },
    {
      checkpointId: "c",
      task: "Do More",
      resourceUrl: "/"
    }
  ],
  tags: ["tic", "tac", "toe"],
  description: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag qui plaid tumeric letterpress. Wolf gentrify live-edge 8-bit. Af ut thundercats locavore williamsburg, blue bottle man braid viral`
};
<Background>
  <CourseCard size="expanded" course={course} />
  <CourseCard size="medium" course={course} />
  <CourseCard size="mini" course={course} />
  <CourseCard size="compact" course={course} />
</Background>;
```

Pass a onCheckpointToggle callback to make it checkable

```js
const course = {
  courseId: "abc",
  goal: "Learn This",
  courseUrl: "/yeehaa",
  curator: "Offcourse",
  checkpoints: [
    {
      checkpointId: "a",
      task: "Do This",
      task: `Gentrify adipisicing fanny pack pabst`,
      resourceUrl: "/"
    },
    {
      checkpointId: "b",
      task: "Do That",
      completed: true,
      resourceUrl: "/"
    },
    {
      checkpointId: "c",
      task: "Do More",
      resourceUrl: "/"
    }
  ],
  tags: ["tic", "tac", "toe"],
  description: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag qui plaid tumeric letterpress. Wolf gentrify live-edge 8-bit. Af ut thundercats locavore williamsburg, blue bottle man braid viral`
};

const onCheckpointToggle = ({ course, checkpointId, complete }) => {
  const checkpoint = course.checkpoints.find(
    ({ checkpointId: cid }) => checkpointId === cid
  );
  const status = complete ? "complete" : "incomplete";
  alert(`You marked "${checkpoint.task}" of "${course.goal}" as ${status}.`);
};

<Background>
  <CourseCard course={course} onCheckpointToggle={onCheckpointToggle} />
</Background>;
```

```js
const curator = "offcourse";
const goal = "manage infrastructure with terraform";
const courseId = "3ca438aa-ea68-42e8-bcaf-1926f67496f6";
<Background>
  <CourseCard curator={curator} goal={goal} />
  <CourseCard courseId={courseId} />
</Background>;
```
