import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Field } from "formik";
import { Input, Icon } from "../../../../atoms";
import CheckpointSectionWrapper from "./CheckpointSectionWrapper";
import { Sortable } from "../../../../utils";

const SortHandle = Sortable.Handle(() => (
  <Icon is="button" name="sort" tabIndex="-1" />
));

const RemoveHandle = ({ onRemove }) => (
  <Icon is="button" tabIndex="-1" name="remove" onClick={onRemove} />
);

class CheckpointSection extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired
  };

  render() {
    const { name, value, onRemove, hasErrors, ...rest } = this.props;
    return (
      <CheckpointSectionWrapper border={hasErrors ? 2 : 0}>
        <Input
          {...rest}
          pb={0}
          mb={0}
          name={`${name}.task`}
          value={value.task}
          placeholder="Task"
        >
          <RemoveHandle onRemove={onRemove} />
        </Input>
        <Input
          {...rest}
          pt={0}
          mt={0}
          name={`${name}.resourceUrl`}
          variant="small"
          value={value.resourceUrl}
          placeholder="Resource URL"
        >
          <SortHandle />
        </Input>
      </CheckpointSectionWrapper>
    );
  }
}

export default CheckpointSection;
