import system from "system-components";

const CheckpointSectionWrapper = system({
  border: 0,
  mb: 3,
  borderColor: "red"
});

export default CheckpointSectionWrapper;
