import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import Form from "../Form";
import { CheckpointSection } from "./sections";
import { Course, Checkpoint } from "../../models";
class CourseForm extends Component {
  render() {
    const { errors, course, onSubmit, onCancel } = this.props;
    const mode = course ? "Edit" : "Create";
    const title = `${mode} Course`;

    return (
      <Form
        schema={Course.schema}
        errors={errors}
        initialValues={course || new Course()}
        onSubmit={onSubmit}
        submitTitle="Save Course"
        onCancel={onCancel}
        title={title}
      >
        {props => {
          return (
            <Fragment>
              <Form.Field
                title="Goal of the Course"
                disabled={mode === "Edit"}
                name="goal"
                placeholder="Goal"
              />

              <Form.FieldList
                title="Checkpoints"
                emptyItem={new Checkpoint()}
                name="checkpoints"
                FieldComponent={CheckpointSection}
              />

              <Form.Field
                title="Description of the Course"
                variant="textarea"
                name="description"
                placeholder="Description"
              />
            </Fragment>
          );
        }}
      </Form>
    );
  }
}

CourseForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  course: PropTypes.shape({
    goal: PropTypes.string.isRequired,
    checkpoints: PropTypes.array,
    description: PropTypes.string
  })
};

CourseForm.defaultProps = {
  onSubmit: () => null,
  onCancel: () => null
};

export default CourseForm;
