import React, { Component } from "react";
import PropTypes from "prop-types";
import { map } from "ramda";
import { CoursesProvider, Masonry } from "../../utils";
import { CourseCard } from "../../organisms";

export default class CourseCards extends Component {
  static propTypes = {
    courses: PropTypes.arrayOf(
      PropTypes.shape({
        courseId: PropTypes.string.isRequired,
        goal: PropTypes.string.isRequired,
        curator: PropTypes.string.isRequired,
        avatarUrl: PropTypes.string,
        courseUrl: PropTypes.string.isRequired
      })
    ),
    cardSize: PropTypes.oneOf(["expanded", "medium", "compact", "mini"]),
    sizes: PropTypes.arrayOf(
      PropTypes.shape({
        mq: PropTypes.string,
        columns: PropTypes.number.isRequired,
        gutter: PropTypes.number.isRequired
      })
    )
  };

  static defaultProps = {
    sizes: [
      { columns: 1, gutter: 20 },
      { mq: "650px", columns: 2, gutter: 20 },
      { mq: "975px", columns: 3, gutter: 20 },
      { mq: "1300px", columns: 4, gutter: 20 },
      { mq: "1625px", columns: 5, gutter: 20 },
      { mq: "1950px", columns: 6, gutter: 20 }
    ],
    cardSize: "medium"
  };

  renderCards(courses) {
    const { cardSize, sizes } = this.props;
    return (
      <Masonry loadMore={() => null} sizes={sizes}>
        {({ forcePack }) => {
          return map(
            course => (
              <CourseCard
                key={course.courseId}
                onResize={forcePack ? forcePack : () => null}
                course={course}
                size={cardSize}
              />
            ),
            courses
          );
        }}
      </Masonry>
    );
  }
  render() {
    const { courses } = this.props;
    if (courses) {
      return this.renderCards(courses);
    }
    return (
      <CoursesProvider>
        {({ courses }) => this.renderCards(courses)}
      </CoursesProvider>
    );
  }
}
