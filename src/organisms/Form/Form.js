import React, { Component } from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import { Link } from "../../atoms";
import { Shell, Field, FieldList, Links } from "./sections";

class Form extends Component {
  render() {
    const {
      children,
      title,
      initialValues,
      linkData,
      schema,
      errors,
      mode,
      messages,
      onCancel,
      submitTitle,
      cancelTitle,
      onSubmit
    } = this.props;

    return (
      <Formik
        validationSchema={schema}
        initialValues={initialValues}
        onSubmit={onSubmit}
      >
        {props => (
          <Shell
            messages={messages}
            externalErrors={errors}
            title={title}
            mode={mode}
            submitTitle={submitTitle}
            cancelTitle={cancelTitle}
            linkData={linkData}
            onCancel={onCancel}
            children={children}
            {...props}
          />
        )}
      </Formik>
    );
  }
}

Form.Field = Field;
Form.FieldList = FieldList;
Form.Link = Link;
Form.Links = Links;

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  schema: PropTypes.object,
  onCancel: PropTypes.func,
  initialValues: PropTypes.object.isRequired,
  children: PropTypes.func.isRequired
};

export default Form;
