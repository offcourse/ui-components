import React, { Component } from "react";
import PropTypes from "prop-types";
import { FieldArray } from "formik";
import { map, values, flatten, identity, filter } from "ramda";
import { InputField, inputList } from "../../../../molecules";
import { Input } from "../../../../atoms";
import Subfield from "./Subfield";
import yup from "yup";

const mapValues = map(values);

export default class FieldList extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    emptyItem: PropTypes.any.isRequired,
    FieldComponent: PropTypes.func
  };

  formatFieldError = error => {
    return yup.object().isType(error)
      ? map(error => error.split(".")[1], values(error))
      : error;
  };

  formatFieldErrors = errors => {
    const flattenedErrors = flatten(errors);
    const filteredErrors = filter(identity, flattenedErrors);
    const formattedErrors = map(this.formatFieldError, filteredErrors);
    const errorsSet = new Set(flatten(formattedErrors));
    return Array.from(errorsSet);
  };

  fieldErrors = errors => {
    if (!errors) {
      return [];
    }

    return yup.string().isType(errors)
      ? [errors]
      : this.formatFieldErrors(errors);
  };

  listErrors = errors => {
    return yup.array().isType(errors) ? map(error => !!error, errors) : [];
  };

  render() {
    const { title, name, placeholder, emptyItem, FieldComponent } = this.props;
    Subfield.FieldComponent = FieldComponent || Input;
    return (
      <FieldArray name={name}>
        {({ form, push, remove, move }) => {
          const { dirty, handleChange, values, touched, errors } = form;
          const add = () => push(emptyItem);
          const items = values[name];
          const fieldErrors = this.fieldErrors(errors[name]);
          const listErrors = this.listErrors(errors[name]);
          return (
            <InputField errors={fieldErrors} name={name} title={title}>
              <InputList
                emptyItem={emptyItem}
                placeholder={placeholder}
                title={title}
                name={name}
                errors={listErrors}
                items={items}
                onRemove={remove}
                onMove={move}
                onAdd={add}
                editable
                FieldComponent={Subfield}
              />
            </InputField>
          );
        }}
      </FieldArray>
    );
  }
}
