import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field } from "formik";

export default class Subfield extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired
  };

  render() {
    const { name, children, ...rest } = this.props;
    return (
      <Field
        name={name}
        render={({ field, form }) => (
          <Subfield.FieldComponent {...rest} {...field} />
        )}
      />
    );
  }
}
