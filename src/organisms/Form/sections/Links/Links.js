import React, { Component } from "react";
import { map, isEmpty } from "ramda";
import { Link, Section } from "../../../../atoms";
import { Group } from "../../../../molecules";
import LinksWrapper from "./LinksWrapper";

class Links extends Component {
  render() {
    const { linkData } = this.props;
    return linkData && !isEmpty(linkData) ? (
      <LinksWrapper>
        <Group vertical links={linkData} />
      </LinksWrapper>
    ) : null;
  }
}

export default Links;
