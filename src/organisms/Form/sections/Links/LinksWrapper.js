import system from "system-components";

const LinksWrapper = system({
  py: "1rem",
  px: "2rem"
});

export default LinksWrapper;
