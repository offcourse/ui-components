import system from "system-components";

const FormWrapper = system({
  is: "form",
  minWidth: "20rem",
  maxWidth: "40rem",
  bg: "white"
});

export default FormWrapper;
