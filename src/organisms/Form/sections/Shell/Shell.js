import React, { Children, Fragment, Component } from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import FormWrapper from "./FormWrapper";
import { Links, Flash, Header, Actions } from "../../sections";

class Shell extends Component {
  state = {};

  static getDerivedStateFromProps(
    { setErrors, mode, setSubmitting, errors, externalErrors },
    { previousErrors, previousMode }
  ) {
    if (externalErrors !== previousErrors) {
      setErrors(externalErrors);
      setSubmitting(false);
    }

    if (mode !== previousMode) {
      setSubmitting(false);
    }

    return {
      previousErrors: externalErrors,
      previousMode: mode
    };
  }

  renderSections() {
    const { children, ...rest } = this.props;
    const rawSections = children(Object.assign({}, ...rest));
    const sections = React.Children.map(rawSections.props.children, child => {
      return child && <Section>{child}</Section>;
    });
    return sections;
  }

  render() {
    const {
      title,
      errors,
      onCancel,
      linkData,
      isSubmitting,
      handleSubmit,
      dirty,
      isValid,
      resetForm
    } = this.props;
    return (
      <FormWrapper onSubmit={handleSubmit}>
        <Flash errors={errors} />
        <Header>{title}</Header>
        {this.renderSections()}
        <Links linkData={linkData} />
        <Actions
          isSubmitting={isSubmitting}
          dirty={dirty}
          isValid={isValid}
          resetForm={resetForm}
          onCancel={onCancel}
        />
      </FormWrapper>
    );
  }
}

Shell.defaultProps = {
  onCancel: () => null,
  mode: "normal"
};

export default Shell;
