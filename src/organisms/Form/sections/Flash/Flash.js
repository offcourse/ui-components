import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import { map, isEmpty, filter, identity } from "ramda";
import titleCase from "voca/title_case";
import { Message } from "../../../../atoms";
import { Group } from "../../../../molecules";
import FlashWrapper from "./FlashWrapper";

const Flash = ({ errors }) => {
  const generalErrors = filter(identity, [errors["general"]]);
  return isEmpty(generalErrors) ? null : (
    <FlashWrapper>
      <Group flex="auto" justifyContent="stretch" errors={generalErrors} />
    </FlashWrapper>
  );
};

export default Flash;
