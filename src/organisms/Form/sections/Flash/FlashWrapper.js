import styled from "styled-components";
import MessageWrapper from "../../../../atoms/Message/MessageWrapper";

const FlashWrapper = styled.div`
  display: "flex";
  flex: 1;
  ${MessageWrapper} {
    padding: 0.5rem 2rem;
  }
`;

export default FlashWrapper;
