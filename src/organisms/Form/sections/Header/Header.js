import React, { Component } from "react";
import PropTypes from "prop-types";
import { Section, Header as AHeader } from "../../../../atoms";

class Header extends Component {
  render() {
    const { children } = this.props;
    return (
      <Section px={8}>
        <AHeader>{children}</AHeader>
      </Section>
    );
  }
}

export default Header;
