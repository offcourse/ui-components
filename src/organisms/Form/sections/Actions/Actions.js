import React, { Component } from "react";
import PropTypes from "prop-types";
import { map, isEmpty, filter, identity } from "ramda";
import { Section, Button } from "../../../../atoms";
import { Group } from "../../../../molecules";

class s extends Component {
  render() {
    const {
      submitTitle,
      cancelTitle,
      onCancel,
      isSubmitting,
      dirty,
      isValid,
      resetForm
    } = this.props;
    return (
      <Section display="flex" justifyContent="flex-end">
        <Group justifyContent="flex-end" direction="horizontal">
          <Button
            disabled={!dirty || isSubmitting}
            onClick={() => {
              onCancel();
              resetForm();
            }}
          >
            {cancelTitle || "Cancel"}
          </Button>
          <Button
            disabled={!isValid || isSubmitting}
            variant="positive"
            type="submit"
          >
            {submitTitle || "Submit"}
          </Button>
        </Group>
      </Section>
    );
  }
}

export default s;
