```js
var yup = require("yup");
const submitHandler = (values, actions) => {
  console.log(values);
  setState(
    {
      errors: {
        testField: "you seem to have misspelled something..."
      }
    },
    actions.setSubmitting(false)
  );
};

const handler = () => alert("Everyting is Erased!!!!");

const subfield = () => "";

const initialValues = {
  testField: "ab",
  subfields: [subfield(), subfield()]
};

const linkData = [
  { handler: handler, title: "Go Somewhere Else" },
  { handler: handler, title: "Go Someplace Other" }
];

const subfieldSchema = yup
  .string()
  .min(3)
  .max(5)
  .required();

const schema = yup.object().shape({
  testField: yup
    .string()
    .min(3)
    .required(),
  subfields: yup
    .array()
    .of(subfieldSchema)
    .min(3)
    .max(4)
});

<Background>
  <Form
    initialValues={initialValues}
    errors={state.errors}
    title="Test Form"
    schema={schema}
    linkData={linkData}
    onSubmit={submitHandler}
    onCancel={handler}
  >
    {props => {
      return (
        <React.Fragment>
          <Form.Field title="Test Field" name="testField" placeholder="Test" />

          <Form.FieldList
            title="Subfields"
            name="subfields"
            placeholder="Subfield"
            emptyItem={subfield()}
            FieldComponent={Input}
          />
        </React.Fragment>
      );
    }}
  </Form>
</Background>;
```
