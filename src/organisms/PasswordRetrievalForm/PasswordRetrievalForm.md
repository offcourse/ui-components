```js
const confirmMode = false;
const errors = {};
const userName = "";
const handler = () => alert("Done!");

<Background>
  <PasswordRetrievalForm
    confirmMode={confirmMode}
    errors={errors}
    userName={userName}
    onSubmit={handler}
    onCancel={handler}
    onRequestSignIn={handler}
  />
</Background>;
```

```js
const confirmMode = true;
const errors = {};
const userName = "";
const handler = () => alert("Done!");

<Background>
  <PasswordRetrievalForm
    confirmMode={confirmMode}
    errors={errors}
    userName={userName}
    onSubmit={handler}
    onCancel={handler}
    onRequestSignIn={handler}
  />
</Background>;
```
