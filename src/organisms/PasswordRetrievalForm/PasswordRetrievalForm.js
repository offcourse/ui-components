import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import Form from "../Form";
import schema from "./passwordRetrievalSchema";

const blankUser = {
  userName: "",
  confirmationCode: "",
  password: ""
};

const PasswordRetrievalForm = ({
  confirmMode,
  userName,
  onSubmit,
  onCancel,
  linkData,
  errors
}) => {
  const mode = confirmMode ? "confirm" : "normal";

  return (
    <Form
      initialValues={userName ? { ...blankUser, userName } : blankUser}
      errors={errors}
      schema={schema}
      mode={mode}
      linkData={linkData}
      onSubmit={onSubmit}
      onCancel={onCancel}
      title="Retrieve Password"
    >
      {props => {
        return (
          <Fragment>
            <Form.Field
              title="User Name"
              name="userName"
              placeholder="User Name"
            />
            {confirmMode && (
              <Form.Field
                title="Confirmation Code"
                name="confirmationCode"
                placeholder="Confirmation Code"
              />
            )}
            {confirmMode && (
              <Form.Field
                title="New Password"
                type="password"
                name="password"
                placeholder="Password"
              />
            )}
          </Fragment>
        );
      }}
    </Form>
  );
};

PasswordRetrievalForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onRequestSignIn: PropTypes.func,
  confirmMode: PropTypes.bool,
  userName: PropTypes.string,
  errors: PropTypes.shape({
    userName: PropTypes.string,
    confirmationCode: PropTypes.string,
    password: PropTypes.string
  })
};

export default PasswordRetrievalForm;
