```js
const handler = () => alert("Done!");
const errors = { general: "you seem to have misspelled something..." };

<Background>
  <SignInForm
    errors={errors}
    onCancel={handler}
    onSubmit={handler}
    onRequestSignIn={handler}
  />
</Background>;
```
