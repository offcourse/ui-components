import React from "react";
import { shallow } from "enzyme";
import SignInForm from "./SignInForm";

describe("Sign Up Form with UserName", () => {
  const handler = jest.fn();
  const userName = "yeehaa";

  const wrapper = shallow(
    <SignInForm
      userName={userName}
      onRequestSignUp={handler}
      onLostPassword={handler}
      onSubmit={handler}
    />
  );

  it("calls the handler with the username", () => {
    wrapper.instance().requestSignUp();
    expect(handler).toBeCalledWith({ userName });
  });

  it("calls the handler with the username", () => {
    wrapper.instance().requestLostPassword();
    expect(handler).toBeCalledWith({ userName });
  });

  it("sets the right initial user", () => {
    const user = wrapper.instance().initialUser();
    expect(user).toEqual({ password: "", userName });
  });

  it("renders the children", () => {
    expect(wrapper.props().children({})).not.isEmptyRender;
  });
});

describe("Sign Up Form witout UserName", () => {
  const handler = jest.fn();

  const wrapper = shallow(
    <SignInForm
      onRequestSignUp={handler}
      onLostPassword={handler}
      onSubmit={handler}
    />
  );

  it("sets the right initial user", () => {
    const user = wrapper.instance().initialUser();
    expect(user).toEqual({ password: "", userName: "" });
  });
});
