import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import Form from "../Form";
import schema from "./signInSchema";
import { map, addIndex } from "ramda";
import titleCase from "voca/title_case";
import { Section, Links } from "../../atoms";

const blankUser = {
  userName: "",
  password: ""
};

class SignInForm extends Component {
  initialUser = () => {
    const { userName } = this.props;
    return userName ? { ...blankUser, userName } : blankUser;
  };

  render() {
    const { onSubmit, linkData, errors, messages, onCancel } = this.props;

    return (
      <Form
        initialValues={this.initialUser()}
        errors={errors}
        messages={messages}
        title="Sign In"
        schema={schema}
        onSubmit={onSubmit}
        onCancel={onCancel}
        linkData={linkData}
      >
        {props => {
          return (
            <Fragment>
              <Form.Field
                title="User Name"
                name="userName"
                placeholder="User Name"
              />
              <Form.Field
                title="Password"
                type="password"
                name="password"
                placeholder="Password"
              />
            </Fragment>
          );
        }}
      </Form>
    );
  }
}

SignInForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onRequestSignUp: PropTypes.func,
  onLostPassword: PropTypes.func,
  userName: PropTypes.string,
  errors: PropTypes.shape({
    userName: PropTypes.string,
    password: PropTypes.string
  })
};

export default SignInForm;
