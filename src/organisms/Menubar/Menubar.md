actions as links

```js
const handler = () => alert("HI");

<Background>
  <Menubar onLogoClick={handler} onMenuClick={handler}>
    <Link onClick={handler}>Create Course</Link>
    <Link onClick={handler}>Profile</Link>
    <Link onClick={handler}>Sign Out</Link>
  </Menubar>
</Background>;
```

actions as prop

```js
const handler = () => alert("HI");

const actions = [
  { handler, title: "Create Course" },
  { handler, title: "Profile" },
  { handler, title: "Sign Out" }
];

<Background>
  <Menubar actions={actions} onLogoClick={handler} onMenuClick={handler} />
</Background>;
```
