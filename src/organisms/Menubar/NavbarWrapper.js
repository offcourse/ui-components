import system from "system-components";
import { justifyContent } from "styled-system/dist/styles";

export default system({
  display: "flex",
  width: "100%",
  height: "auto",
  flexDirection: "column"
}).extend`
`;
