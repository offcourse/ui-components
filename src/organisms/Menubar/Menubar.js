import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Logo, Button, Icon } from "../../atoms";
import { Group } from "../../molecules";
import { Menu } from "./sections";
import Sidebar from "react-sidebar";

import MenubarWrapper from "./MenubarWrapper";
import NavbarWrapper from "./NavbarWrapper";

const stylesOverrides = {
  root: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: "hidden"
  },
  sidebar: {
    zIndex: 4,
    position: "absolute",
    top: 0,
    bottom: 0,
    backgroundColor: "white",
    opacity: 1,
    padding: "1rem",
    width: "15rem",
    transition: "transform .3s ease-out",
    WebkitTransition: "-webkit-transform .3s ease-out",
    willChange: "transform",
    overflowY: "auto"
  },
  content: {
    position: "absolute",
    padding: "1rem",
    paddingTop: "2.875rem",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflowY: "scroll",
    WebkitOverflowScrolling: "touch",
    transition: "left .3s ease-out, right .3s ease-out"
  },
  overlay: {
    zIndex: 3,
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: 0,
    visibility: "hidden",
    transition: "opacity .3s ease-out, visibility .3s ease-out",
    backgroundColor: "rgba(0,0,0,.3)"
  },
  dragHandle: {
    zIndex: 1,
    position: "fixed",
    top: 0,
    bottom: 0
  }
  // overlay: { zIndex: 1 },
  // sidebar: {
  //   zIndex: 2,
  //   padding: "1rem",
  //   width: "15rem",
  // }
};

class Menubar extends Component {
  state = { isOpen: false };
  renderActions() {
    const { children, actions } = this.props;
    const groupProps = {
      justifyContent: "flex-end",
      direction: "horizontal",
      spacing: "wide"
    };
    return actions ? (
      <Group {...groupProps} links={actions} />
    ) : (
      <Group {...groupProps}>{children}</Group>
    );
  }

  renderMobileMenu() {
    return (
      <Group direction="horizontal" justifyContent="flex-end">
        <Icon onClick={this.toggleSidebar} size="lg" name="hamburger" />
      </Group>
    );
  }

  toggleSidebar = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  render() {
    const { children, onLogoClick, onMenuClick, actions } = this.props;
    const { isOpen } = this.state;
    return (
      <Sidebar
        shadow={false}
        open={isOpen}
        styles={stylesOverrides}
        sidebar={<Menu actions={actions} />}
        onSetOpen={this.toggleSidebar}
      >
        <MenubarWrapper>
          <Logo onClick={onLogoClick} />
          {this.renderMobileMenu()}
        </MenubarWrapper>
        {children}
      </Sidebar>
    );
  }
}

Menubar.propTypes = {
  onLogoClick: PropTypes.func,
  onMenuClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
};

export default Menubar;
