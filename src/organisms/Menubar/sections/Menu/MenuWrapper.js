import { keyframes } from "styled-components";
import system from "system-components";

export default system({
  display: "flex",
  position: "fixed",
  top: 0,
  left: 0,
  right: 0,
  px: 4,
  pb: 6,
  pt: 4,
  bg: "white",
  borderBottom: 1,
  borderColor: "primary",
  width: "100%",
  zIndex: 10
}).extend`
`;
