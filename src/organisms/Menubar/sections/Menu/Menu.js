import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Group } from "../../../../molecules";
import sizeMe from "react-sizeme";
import MenuWrapper from "./MenuWrapper";

class Menu extends Component {
  render() {
    const { size, actions, children, isOpen } = this.props;
    const props = {
      direction: "vertical",
      spacing: "wide",
      flex: 1,
      display: "flex"
    };
    return actions ? (
      <Group {...props} links={actions} />
    ) : (
      <Group {...props}>{children}</Group>
    );
  }
}

export default sizeMe({ monitorHeight: true })(Menu);
