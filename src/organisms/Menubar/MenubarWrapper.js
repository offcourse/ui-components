import system from "system-components";

export default system({
  display: "flex",
  position: "fixed",
  top: 0,
  left: 0,
  right: 0,
  justifyContent: "space-between",
  bg: "white",
  flexDirection: "row",
  pr: 4,
  alignItems: "center",
  height: "1.875rem",
  zIndex: 2
}).extend`
  box-sizing: border-box
`;
