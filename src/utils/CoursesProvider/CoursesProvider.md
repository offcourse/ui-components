```js
<div style={{ backgroundColor: "#f4f6f4", padding: "1em", margin: "-1rem" }}>
  <CoursesProvider>
    {({ courses }) =>
      courses.map(course => <div key={course.courseId}>{course.goal}</div>)
    }
  </CoursesProvider>
</div>
```
