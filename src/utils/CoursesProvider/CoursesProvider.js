import React, { Component } from "react";
import slugify from "voca/slugify";
import ApolloClient from "apollo-boost";
import { ApolloProvider, Query } from "react-apollo";
import { map } from "ramda";
import { Loading } from "../../atoms";
import gql from "graphql-tag";

const client = new ApolloClient({
  uri: "https://api.offcourse.io/graphql",
  request: async operation => {
    const token = "GUEST";
    operation.setContext({
      headers: {
        authorization: token
      }
    });
  }
});
const courses = gql`
  {
    courses {
      edges {
        node {
          goal
          curator
          timestamp
          courseId
          description
          revision
          tags
          checkpoints {
            task
            checkpointId
            resourceUrl
          }
        }
      }
    }
  }
`;

const filterTags = oldTags => {
  const tags = new Set(oldTags.filter(t => t && t));
  return Array.from(tags);
};

export default ({ children }) => (
  <ApolloProvider client={client}>
    <Query query={courses}>
      {({ loading, error, data }) => {
        const baseUrl = "https://app.offcourse.io";
        if (loading) return <Loading size="5x" />;
        if (error) return <Debug data={error} />;
        return children({
          courses: map(
            ({ node }) => ({
              ...node,
              tags: filterTags(node.tags),
              courseUrl: `${baseUrl}/users/${slugify(
                node.curator
              )}/courses/${slugify(node.goal)}`
            }),
            data.courses.edges
          )
        });
      }}
    </Query>
  </ApolloProvider>
);
