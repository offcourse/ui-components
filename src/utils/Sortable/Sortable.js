import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import {
  SortableHandle,
  SortableContainer,
  SortableElement,
  arrayMove
} from "react-sortable-hoc";

class InnerElement extends Component {
  render() {
    const { children } = this.props;
    return children;
  }
}

const Element = SortableElement(InnerElement);
Element.displayName = `SortElement`;

class Sortable extends Component {
  render() {
    const { Component, children, ...rest } = this.props;
    return (
      <Component {...rest} sortable>
        return <Element index={index}>{child}</Element>; })}
      </Component>
    );
  }
}

const Sorter = SortableContainer(Sortable);

Sorter.Handle = SortableHandle;
Sorter.move = arrayMove;
Sorter.displayName = `Sortable`;
export default Sorter;
