```js
initialState = {
  items: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"]
};
const onSortEnd = ({ oldIndex, newIndex }) => {
  setState({
    items: Sortable.move(state.items, oldIndex, newIndex)
  });
};
<div>
  <Sortable
    onSortEnd={onSortEnd}
    Component={List}
    variant="checkbox"
    useDragHandle={true}
    Handle={Sortable.Handle}
    onCheckboxToggle={({ id, checked }) =>
      alert(`this id of this item is: ${id}, its checked status is ${checked}`)
    }
  >
    {state.items.map((item, index) => (
      <List.Item key={index}>{item}</List.Item>
    ))}
  </Sortable>
</div>;
```
