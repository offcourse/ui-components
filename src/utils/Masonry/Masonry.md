```js
const course = {
  courseId: "abc",
  goal: "Learn This",
  curator: "Offcourse",
  courseUrl: "/yeehaa",
  profileUrl: `/curator/yeehaa`,
  checkpoints: [
    {
      checkpointId: "a",
      task: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag quo`,
      resourceUrl: "/"
    },
    {
      checkpointId: "b",
      task: "Do That",
      completed: true,
      resourceUrl: "/"
    },
    {
      checkpointId: "c",
      task: "Do More",
      resourceUrl: "/"
    }
  ],
  tags: ["tic", "tac", "toe"],
  description: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag qui plaid tumeric letterpress. Wolf gentrify live-edge 8-bit. Af ut thundercats locavore williamsburg, blue bottle man braid viral`
};

const sizes = [
  { columns: 1, gutter: 20 },
  { mq: "650px", columns: 2, gutter: 20 }
];

<Background>
  <Masonry loadMore={() => null} sizes={sizes}>
    {({ forcePack }) => {
      return [
        { course, size: "compact" },
        { course, size: "expanded" },
        { course, size: "mini" },
        { course, size: "medium" }
      ].map(({ course, size }, index) => (
        <CourseCard
          key={index}
          onResize={forcePack}
          course={course}
          size="mini"
        />
      ));
    }}
  </Masonry>
</Background>;
```
