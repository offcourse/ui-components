CourseQuery by Curator and Goal

```js
const curator = "offcourse";
const goal = "manage infrastructure with terraform";
<div style={{ backgroundColor: "#f4f6f4", padding: "1em", margin: "-1rem" }}>
  <CourseProvider courseQuery={{ curator, goal }}>
    {({ course }) => (
      <div>
        <div>{course.goal}</div>
        <div>{course.curator}</div>
        <div>{course.description}</div>
      </div>
    )}
  </CourseProvider>
</div>;
```

CourseQuery by CourseId

```js
const courseId = "3ca438aa-ea68-42e8-bcaf-1926f67496f6";
<div style={{ backgroundColor: "#f4f6f4", padding: "1em", margin: "-1rem" }}>
  <CourseProvider courseId={courseId}>
    {({ course }) => (
      <div>
        <div>{course.goal}</div>
        <div>{course.curator}</div>
        <div>{course.description}</div>
      </div>
    )}
  </CourseProvider>
</div>;
```
