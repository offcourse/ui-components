import React, { Component } from "react";
import PropTypes from "prop-types";
import slugify from "voca/slugify";
import Course from "../../organisms/CourseCard";
import { createApolloFetch } from "apollo-fetch";

const dummy = status => {
  return {
    courseId: "temp",
    goal: `${status}...`,
    curator: `${status}`,
    courseUrl: "loading...",

    checkpoints: [
      {
        checkpointId: "a",
        task: `${status}...`,
        resourceUrl: "/"
      },
      {
        checkpointId: "b",
        task: `${status}...`,
        completed: true,
        resourceUrl: "/"
      },
      {
        checkpointId: "c",
        task: `${status}...`,
        resourceUrl: "/"
      }
    ],
    tags: [`${status}.`, `${status}..`, `${status}...`],
    description: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag qui plaid tumeric letterpress. Wolf gentrify live-edge 8-bit. Af ut thundercats locavore williamsburg, blue bottle man braid viral`
  };
};

const byCourseId = `
  query Course($courseId: String!) {
    course(courseId: $courseId) {
      goal
      curator
      timestamp
      courseId
      description
      revision
      tags
      checkpoints {
        task
        checkpointId
        resourceUrl
      }
    }
  }
`;

const byCourseQuery = `
  query Course($courseQuery: CourseQuery!) {
    course(courseQuery: $courseQuery) {
      goal
      curator
      timestamp
      courseId
      description
      revision
      tags
      checkpoints {
        task
        checkpointId
        resourceUrl
      }
    }
  }
`;

class CourseProvider extends Component {
  state = { course: dummy("Loading") };

  filterTags = oldTags => {
    const tags = new Set(oldTags.filter(t => t && t));
    return Array.from(tags);
  };

  componentDidMount() {
    const { courseQuery, courseId, endpoint, authorizationToken } = this.props;
    const apolloFetch = createApolloFetch({ uri: endpoint });

    const variables = courseId ? { courseId } : { courseQuery };
    const query = courseId ? byCourseId : byCourseQuery;
    const baseUrl = "https://app.offcourse.io";

    apolloFetch.use(({ request, options }, next) => {
      if (!options.headers) {
        options.headers = {};
      }
      options.headers["authorization"] = authorizationToken;

      next();
    });

    // this needs to be changed on backend... temp fix
    apolloFetch({ query, variables })
      .then(data => {
        const { course } = data.data;
        if (course) {
          this.setState({
            course: {
              ...course,
              courseUrl: `${baseUrl}/users/${slugify(
                course.curator
              )}/courses/${slugify(course.goal)}`,
              tags: this.filterTags(course.tags)
            }
          });
        } else {
          this.setState({ course: dummy("Not Found") });
        }
      })
      .catch(e => this.setState({ course: dummy("Oops") }));
  }

  render() {
    const { children } = this.props;
    const { course } = this.state;
    return children({ course });
  }
}

CourseProvider.propTypes = {
  courseQuery: PropTypes.shape({
    goal: PropTypes.string.isRequired,
    curator: PropTypes.string.isRequired
  }),
  CourseId: PropTypes.shape({
    courseId: PropTypes.string.isRequired
  }),
  endpoint: PropTypes.string,
  authorizationToken: PropTypes.string
};

CourseProvider.defaultProps = {
  authorizationToken: "GUEST",
  endpoint: "https://api.offcourse.io/graphql"
};

export default CourseProvider;
