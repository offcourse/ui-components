```js
const handler = data => alert(JSON.stringify(data, null, 2));
<AuthProvider onSignIn={handler} onSignOut={handler}>
  {({ signIn, signUp, resetPassword, signOut, authData }) => {
    const {
      authStatus,
      errors,
      userName,
      accessToken,
      refreshToken
    } = authData;
    return authStatus !== "SIGNED_IN" ? (
      <Auth
        errors={errors}
        signIn={signIn}
        signUp={signUp}
        resetPassword={resetPassword}
      />
    ) : (
      <Card>
        <div>
          <Label>User Name</Label>
          <Header>{userName}</Header>
        </div>
        <Button onClick={signOut}>Sign Out</Button>
      </Card>
    );
  }}
</AuthProvider>;
```

```js
const handler = data => alert(JSON.stringify(data, null, 2));
<AuthProvider onSignIn={handler} onSignOut={handler}>
  {({ signIn, signOut, authData }) => {
    const {
      authStatus,
      errors,
      userName,
      accessToken,
      refreshToken
    } = authData;
    return authStatus !== "SIGNED_IN" ? (
      <SignInForm errors={errors} onSubmit={signIn} />
    ) : (
      <Card>
        <div>
          <Label>User Name</Label>
          <Header>{userName}</Header>
        </div>
        <Button onClick={signOut}>Sign Out</Button>
      </Card>
    );
  }}
</AuthProvider>;
```

```js
const handler = data => alert(JSON.stringify(data, null, 2));
<AuthProvider onSignIn={handler} onSignOut={handler}>
  {({ signUp, signOut, authData }) => {
    const {
      authStatus,
      errors,
      userName,
      accessToken,
      needsConfirmation,
      refreshToken
    } = authData;
    return authStatus !== "SIGNED_IN" ? (
      <SignUpForm
        confirmMode={needsConfirmation}
        errors={errors}
        onSubmit={signUp}
      />
    ) : (
      <Card>
        <div>
          <Label>User Name</Label>
          <Header>{userName}</Header>
        </div>
        <div>
          <Button onClick={signOut}>Sign Out</Button>
        </div>
      </Card>
    );
  }}
</AuthProvider>;
```

```js
const handler = data => alert(JSON.stringify(data, null, 2));
<AuthProvider onSignIn={handler} onSignOut={handler}>
  {({ resetPassword, authData }) => {
    const {
      authStatus,
      errors,
      userName,
      accessToken,
      needsConfirmation,
      signOut,
      refreshToken
    } = authData;
    return authStatus !== "SIGNED_IN" ? (
      <PasswordRetrievalForm
        confirmMode={needsConfirmation}
        errors={errors}
        onSubmit={resetPassword}
      />
    ) : (
      <Card>
        <div>
          <Label>User Name</Label>
          <Header>{userName}</Header>
        </div>
        <div>
          <Button onClick={signOut}>Sign Out</Button>
        </div>
      </Card>
    );
  }}
</AuthProvider>;
```
