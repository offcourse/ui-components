import React from "react";
import { shallow } from "enzyme";
import { forEach } from "ramda";
import Expander from "./Expander";

describe("with all sections defined", () => {
  const hiddenFields = {
    expanded: [],
    medium: ["ho"],
    compact: ["ha", "ho"]
  };
  const onResize = jest.fn();
  const wrapper = shallow(
    <Expander onResize={onResize} hiddenFields={hiddenFields}>
      <div expand="onClick" className="expander" section="hi" />
      <div section="ha">HA</div>
      <div section="ho">HO</div>
    </Expander>
  );

  it("defaults to the first key of hiddenFields", () => {
    const { currentSize } = wrapper.instance();
    expect(currentSize()).toBe("expanded");
  });

  it("cycles through states", () => {
    wrapper.find(".expander").simulate("click");
    expect(onResize).toBeCalledWith({ size: "medium" });
    expect(wrapper.find("div").length).toBe(2);

    onResize.mockReset();
    wrapper.find(".expander").simulate("click");
    expect(onResize).toBeCalledWith({ size: "compact" });
    expect(wrapper.find("div").length).toBe(1);

    onResize.mockReset();
    wrapper.find(".expander").simulate("click");
    expect(onResize).toBeCalledWith({ size: "expanded" });
    expect(wrapper.find("div").length).toBe(3);
  });

  it("size can be overriden through props", () => {
    const wrapper = shallow(
      <Expander size="compact" hiddenFields={hiddenFields}>
        <div>HI</div>
        <div>HA</div>
        <div>HO</div>
      </Expander>
    );
    const { currentSize } = wrapper.instance();
    expect(currentSize()).toBe("compact");
  });

  describe("when used without hidden props it acts as a normal element", () => {
    const wrapper = shallow(
      <Expander>
        <div>HI</div>
        <div>HA</div>
        <div>HO</div>
      </Expander>
    );

    it("has the right length", () => {
      const { currentSize } = wrapper.instance();
      expect(currentSize()).toBe("default");
      expect(wrapper.find("div").length).toBe(3);
    });
  });
});
