import React, { Component } from "react";
import classNames from "classnames";
import { indexOf, keys, addIndex, reduce, contains } from "ramda";
import PropTypes from "prop-types";

/**
 * Generic card component for the Offcourse project
 */

const reduceIndexed = addIndex(reduce);

class Expander extends Component {
  state = {
    size: this.props.size || keys(this.props.hiddenFields)[0] || "default"
  };

  currentSize = () => {
    return this.state.size;
  };

  allSizes = () => keys(this.props.hiddenFields);

  nextSize = () => {
    const sizes = this.allSizes();
    const currentIndex = sizes && indexOf(this.currentSize(), sizes);
    const isLast = currentIndex + 1 >= sizes.length;
    return isLast ? sizes[0] : sizes[currentIndex + 1];
  };

  handleResize = () => {
    this.setState(
      ({ size }) => ({
        size: this.nextSize()
      }),
      () => {
        const { onResize } = this.props;
        return onResize({
          size: this.currentSize()
        });
      }
    );
  };

  sections = ({ children, hiddenFields, currentSize }) => {
    const fields = hiddenFields[this.currentSize()];
    const sections = reduceIndexed(
      (acc, section, index) => {
        const isHidden = fields && contains(section.props.section, fields);
        const { expand, ...rest } = section.props;
        const key = section.props.section || index;
        const newSection = expand
          ? React.cloneElement(section, {
              key,
              ...rest,
              [expand]: this.handleResize
            })
          : React.cloneElement(section, {
              key,
              ...rest
            });
        return isHidden ? acc : [...acc, newSection];
      },
      [],
      children
    );
    return sections;
  };

  render() {
    const { children, hiddenFields } = this.props;
    const currentSize = this.currentSize();
    return (
      <React.Fragment>
        {this.sections({ children, hiddenFields, currentSize })}
      </React.Fragment>
    );
  }
}

Expander.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  size: PropTypes.string,
  onResize: PropTypes.func,
  hiddenFields: PropTypes.object
};

Expander.defaultProps = {
  onResize: () => {},
  hiddenFields: {}
};

export default Expander;
