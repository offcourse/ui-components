Designed by [Donald Roos](http://bureaudonald.com)

## Color Palette

```js
<Swatch color="primary" />
<Swatch color="disabled" />
<Swatch color="positive" />
<Swatch color="warning" />
<Swatch color="info" />
<Swatch color="negative" />
```

## Grayscale

```js
<Swatch color="grayScale.0" />
<Swatch color="grayScale.1" />
<Swatch color="grayScale.2" />
<Swatch color="grayScale.3" />
<Swatch color="grayScale.4" />
```

## Header Font

```js
<Header>Nitti Grotesk 700</Header>
<Header size="small">Nitti Grotesk 700</Header>
```

## Label Font

```js
<Label>Nitti Grotesk 500</Label>
```

## Basefont

```js
<Text>Nitti Grotesk 500</Text>
<Text size="small">Nitti Grotesk 500</Text>
```

## Logo

```js
<Logo />
```
