import React, { Component } from "react";
import theme from "./src/themes/default";
import { injectGlobal, ThemeProvider } from "styled-components";

export default class ThemeWrapper extends Component {
  render() {
    const { children } = this.props;
    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
  }
}
