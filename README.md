# Offcourse UI Components

Reusable UI Components for the Offcourse Project.

## Install and Use

To use them in your React App run:

```bash
npm install ofcourse-ui-components
```

or add the library to your package.json.

The npm package also includes a umd build that can be added to your package.json.

```html
<script src="offcourse-ui-components.js"/>
```
